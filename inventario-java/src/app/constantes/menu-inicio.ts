import {DatosCardInterface} from '../componentes/card-menu/card-menu.component';

export const MENU_INICIO: DatosCardInterface[]= [
  {
    link: '/departamentos',
    titulo: 'DEPARTAMENTOS',
    detalle:'Gestione DEPARTAMENTOS de su empresa',
    icono: 'department.svg'
  },
  {
    link: '/bodegas',
    titulo: 'BODEGAS',
    detalle:'Gestione BODEGAS de su empresa',
    icono: 'shop.svg'
  },
  {
    link: '/productos',
    titulo: 'PRODUCTOS',
    detalle:'Gestione PRODUCTOS de su empresa',
    icono: 'producto.svg'
  },
  {
    link: '/usuarios',
    titulo: 'USUARIOS',
    detalle:'Gestione USUARIOS de su empresa',
    icono: 'usuario.svg'
  },
  {
    link: '/reportes',
    titulo: 'REPORTES',
    detalle:'Visualice reportes de mantenimiento',
    icono: 'reporte.svg'
  }

]
