export enum EstadosMantenimientoEnum {
  PR= 'Por recibir' ,
  ER= 'En revisión'  ,
  PE= 'Por entregar'  ,
  EN= 'Entregado',
  NR = 'No realizado'
}
