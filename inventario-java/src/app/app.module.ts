import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {OpcionesMenuComponent} from './componentes/opciones-menu/opciones-menu.component';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {RutaLoginComponent} from './rutas/ruta-login/ruta-login.component';
import {RutaNoEncontradoComponent} from './rutas/ruta-no-encontrado/ruta-no-encontrado.component';
import {RutaInicioComponent} from './rutas/ruta-inicio/ruta-inicio.component';
import {CargandoService} from './servicios/cargando.service';
import {BlockUIModule} from 'ng-block-ui';
import {MatCardModule} from '@angular/material/card';
import {ToolbarModule} from 'primeng/toolbar';
import {BodegaInventarioRestService} from './servicios/rest/bodega-inventario-rest.service';
import {BodegaRestService} from './servicios/rest/bodega-rest.service';
import {DepartamentoRestService} from './servicios/rest/departamento-rest.service';
import {MantenimientoRestService} from './servicios/rest/mantenimiento-rest.service';
import {ProductoRestService} from './servicios/rest/producto-rest.service';
import {UsuarioRestService} from './servicios/rest/usuario-rest.service';
import {HttpClientModule} from '@angular/common/http';
import {CardMenuComponent} from './componentes/card-menu/card-menu.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MODULOS_COMUNES} from './constantes/modulos-comunes';
import {ToastrModule} from 'ngx-toastr';
import {LoginService} from './servicios/login.service';
import {EstaLogeadoGuard} from './servicios/guards/esta-logeado.guard';
import {GestionaBodegasGuard} from './servicios/guards/gestiona-bodegas.guard';
import {DaMantenimientoGuard} from './servicios/guards/da-mantenimiento.guard';
import {GestionaDepartamentosGuard} from './servicios/guards/gestiona-departamentos.guard';
import {GestionaInventariosGuard} from './servicios/guards/gestiona-inventarios.guard';
import {GestionaProductosGuard} from './servicios/guards/gestiona-productos.guard';
import {VeReportesGuard} from './servicios/guards/ve-reportes.guard';
import {GestionaUsuariosGuard} from './servicios/guards/gestiona-usuarios.guard';

@NgModule({
  declarations: [
    AppComponent,
    OpcionesMenuComponent,
    RutaLoginComponent,
    RutaNoEncontradoComponent,
    RutaInicioComponent,
    CardMenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatDividerModule,
    BlockUIModule.forRoot(),
    MatCardModule,
    ToolbarModule,
    HttpClientModule,
    MatFormFieldModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    ...MODULOS_COMUNES,
    ToastrModule.forRoot(),
  ],
  providers: [
    CargandoService,
    BodegaInventarioRestService,
    BodegaRestService,
    DepartamentoRestService,
    MantenimientoRestService,
    ProductoRestService,
    UsuarioRestService,
    LoginService,
    EstaLogeadoGuard,
    GestionaBodegasGuard,
    DaMantenimientoGuard,
    GestionaDepartamentosGuard,
    GestionaInventariosGuard,
    GestionaProductosGuard,
    VeReportesGuard,
    GestionaUsuariosGuard
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
