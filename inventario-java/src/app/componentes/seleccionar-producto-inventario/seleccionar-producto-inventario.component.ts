import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProductoInterface} from "../../modulos/producto/interfaces/producto.interface";
import {ProductoRestService} from "../../servicios/rest/producto-rest.service";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {map, startWith} from "rxjs/operators";
import {CargandoService} from '../../servicios/cargando.service';
import {ArticuloInventarioInterface} from '../../modulos/inventario/interfaces/articulo-inventario.interface';
import {ArticuloInventarioRestService} from '../../servicios/rest/articulo-inventario-rest.service';

@Component({
  selector: 'app-seleccionar-producto-inventario',
  templateUrl: './seleccionar-producto-inventario.component.html',
  styleUrls: ['./seleccionar-producto-inventario.component.scss']
})
export class SeleccionarProductoInventarioComponent implements OnInit {

  @Output()
  seleccionoProducto: EventEmitter<ArticuloInventarioInterface> = new EventEmitter<ArticuloInventarioInterface>();
  arregloProductos: ArticuloInventarioInterface[] = [];
  productoSeleccionado?: ArticuloInventarioInterface;

  controlAutocomplete = new FormControl();
  opcionesFiltradas: Observable<ArticuloInventarioInterface[]> = new Observable<ArticuloInventarioInterface[]>();

  constructor(
    private readonly _articuloInventarioRestService: ArticuloInventarioRestService,
    private readonly _cargandoService: CargandoService
  ) {
  }

  ngOnInit(): void {
    // this._cargarProductos('');
    this.escucharCambiosAutocomplete();

  }

  public _cargarProductos(busquedaParametro: any) {

    console.log('busqueda parametros', busquedaParametro)
    this._cargandoService.habilitarCargando();
    const busqueda = {
      busqueda: busquedaParametro.query,
      skip: 0,
      take: 10000
    };
    this._articuloInventarioRestService
      .buscar(busqueda)
      .subscribe(
        (r: any) => {
          this._cargandoService.deshabilitarCargando();
          this.arregloProductos = r.listaObjetos;
        },
        error => {
          this._cargandoService.deshabilitarCargando();
          console.error('Ocurrio un error', error);
        }
      );

  }

  emitirSeleccionoTransporte(valor?: ArticuloInventarioInterface) {
    console.log('sexo a emitir', valor)
    this.seleccionoProducto.emit(valor);
  }

  escucharCambiosAutocomplete() {
    this.opcionesFiltradas = this.controlAutocomplete
      .valueChanges
      .pipe(
        startWith(''),
        map(value => {
          if(value === null) {
            value= ''
          }
            console.log('value autocomplete', value);
            return this._cargarProductos(value);
          },
          ),
        map((valores: any) => {
          console.log('valores encontrados exactos', valores);
          if(valores){
            this.arregloProductos =  valores.listaObjetos;
            return this.arregloProductos;
          }else {
            return []
          }

        })
      );
  }

  private _filtrarArreglo(value: string): ArticuloInventarioInterface[] {
    const valorFiltrado = value;
    return this.arregloProductos
      .filter(option => {
        const opcionNombre = option['nombre'] as string;
        return opcionNombre.search(valorFiltrado) >= 0;
      });
  }

  mostrarNombreCooperativa() {
    return (val: any) => this.display(val);
  }

  private display(producto: ArticuloInventarioInterface): any {
    return producto ? `${producto.codigo}-${producto.nombre}` : producto;
  }

}
