import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MensajeConfirmacionComponent } from './mensaje-confirmacion/mensaje-confirmacion.component';
import {TituloRutaModule} from '../titulo-ruta/titulo-ruta.module';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [MensajeConfirmacionComponent],
  imports: [
    CommonModule,
    TituloRutaModule,
    MatCardModule,
    MatButtonModule
  ],
  entryComponents: [
    MensajeConfirmacionComponent
  ]
})
export class MensajeConfirmacionModule { }
