import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-mensaje-confirmacion',
  templateUrl: './mensaje-confirmacion.component.html',
  styleUrls: ['./mensaje-confirmacion.component.scss']
})
export class MensajeConfirmacionComponent implements OnInit {

  mensaje = '¿Está <strong>seguro</strong> de la accion a realizar?';

  constructor(
    private readonly _matDialogRef: MatDialogRef<MensajeConfirmacionComponent>,
    @Inject(MAT_DIALOG_DATA) private readonly data: { mensaje: string }
  ) {
  }

  ngOnInit(): void {
    if (this.data.mensaje) {
      this.mensaje = this.data.mensaje;
    }
  }

  cerrarMensajeConfirmacion(respuesta: boolean) {
    this._matDialogRef.close(respuesta);
  }

}
