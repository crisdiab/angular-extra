import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProductoInterface} from "../../modulos/producto/interfaces/producto.interface";
import {ProductoRestService} from "../../servicios/rest/producto-rest.service";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {map, startWith} from "rxjs/operators";
import {CargandoService} from '../../servicios/cargando.service';

@Component({
  selector: 'app-seleccionar-producto',
  templateUrl: './seleccionar-producto.component.html',
  styleUrls: ['./seleccionar-producto.component.scss']
})
export class SeleccionarProductoComponent implements OnInit {

  @Output()
  seleccionoProducto: EventEmitter<ProductoInterface> = new EventEmitter<ProductoInterface>();

  arregloProductos: ProductoInterface[] = [];
  productoSeleccionado?: ProductoInterface;

  controlAutocomplete = new FormControl();
  opcionesFiltradas: Observable<ProductoInterface[]> = new Observable<ProductoInterface[]>();

  constructor(
    private readonly _productoRestService: ProductoRestService,
    private readonly _cargandoService: CargandoService
  ) {
  }

  ngOnInit(): void {
    this._cargarProductos();
    this.escucharCambiosAutocomplete();

  }

  private _cargarProductos() {
    this._cargandoService.habilitarCargando();
    const busqueda = {
      busqueda: '',
      skip: 0,
      take: 10000
    }
    this._productoRestService
      .buscar(busqueda)
      .subscribe(
        (r: any) => {
          this._cargandoService.deshabilitarCargando();
          this.arregloProductos = r.listaObjetos;
        },
        error => {
          this._cargandoService.deshabilitarCargando();
          console.error('Ocurrio un error', error)
        }
      );

  }

  emitirSeleccionoTransporte(valor?: ProductoInterface) {
    this.seleccionoProducto.emit(valor);
  }

  escucharCambiosAutocomplete() {
    this.opcionesFiltradas = this.controlAutocomplete
      .valueChanges
      .pipe(
        startWith(''),
        map(value => this._filtrarArreglo(value))
      );
  }

  private _filtrarArreglo(value: string): ProductoInterface[] {
    const valorFiltrado = value;
    return this.arregloProductos
      .filter(option => {
        const opcionNombre = option['nombre'] as string;
        return opcionNombre.search(valorFiltrado) >= 0
      });
  }

  mostrarNombreCooperativa() {
    return (val: any) => this.display(val);
  }

  private display(producto: ProductoInterface): any {
    return producto ? `${producto.codigo}-${producto.nombre}` : producto;
  }

}
