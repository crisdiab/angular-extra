import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SeleccionarProductoComponent} from './seleccionar-producto.component';
import {MatCardModule} from '@angular/material/card';
import {TituloRutaModule} from '../titulo-ruta/titulo-ruta.module';
import {TableModule} from 'primeng/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';
import {ToolbarModule} from 'primeng/toolbar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ActivoInactivoPipeModule} from '../../pipes/activo-inactivo-pipe/activo-inactivo-pipe.module';
import {EstadoMantenimientoPipeModule} from '../../pipes/estado-mantenimiento-pipe/estado-mantenimiento-pipe.module';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatSelectModule} from '@angular/material/select';
import {CalendarModule} from 'primeng/calendar';
import {MatOptionModule} from '@angular/material/core';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {InputTextModule} from 'primeng/inputtext';



@NgModule({
  declarations: [
    SeleccionarProductoComponent,
  ],
  imports: [
    CommonModule,
    MatCardModule,
    TituloRutaModule,
    TableModule,
    MatFormFieldModule,
    MatButtonModule,
    ButtonModule,
    RippleModule,
    ToolbarModule,
    MatDialogModule,
    MatInputModule,
    MatIconModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatOptionModule,
    MatAutocompleteModule,
    InputTextModule,
  ],
  exports: [
    SeleccionarProductoComponent,
  ]
})
export class SeleccionarProductoModule { }
