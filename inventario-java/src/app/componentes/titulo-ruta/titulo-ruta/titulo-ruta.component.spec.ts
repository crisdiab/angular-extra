import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TituloRutaComponent } from './titulo-ruta.component';

describe('TituloRutaComponent', () => {
  let component: TituloRutaComponent;
  let fixture: ComponentFixture<TituloRutaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TituloRutaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TituloRutaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
