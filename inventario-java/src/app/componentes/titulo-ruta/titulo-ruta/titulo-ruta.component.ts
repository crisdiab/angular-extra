import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-titulo-ruta',
  templateUrl: './titulo-ruta.component.html',
  styleUrls: ['./titulo-ruta.component.scss']
})
export class TituloRutaComponent implements OnInit {

  @Input()
  titulo = '';

  @Input()
  descripcion = '';

  @Input()
  iconoUrl = '';

  @Input()
  mostrarIrAtras = true;

  constructor() {
  }

  ngOnInit(): void {
  }

  irAtras() {
    window.history.back();
  }

}
