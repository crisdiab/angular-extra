import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TituloRutaComponent} from './titulo-ruta/titulo-ruta.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    TituloRutaComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
  ],
  exports: [
    TituloRutaComponent
  ]
})
export class TituloRutaModule {
}
