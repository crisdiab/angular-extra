import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card-menu',
  templateUrl: './card-menu.component.html',
  styleUrls: ['./card-menu.component.scss']
})
export class CardMenuComponent implements OnInit {

  @Input() datosCard?: DatosCardInterface
  constructor() { }

  ngOnInit(): void {
  }

}

export interface DatosCardInterface {
  link?: string;
  titulo?: string;
  icono?: string;
  detalle?: string;
}
