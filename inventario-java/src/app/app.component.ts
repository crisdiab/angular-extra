import {Component, OnInit} from '@angular/core';
import {CargandoService} from './servicios/cargando.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {LoginService} from './servicios/login.service';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'inventario-java';
  @BlockUI() blockUI: any;
  enviroment = environment;
  nombreUsuario?: string;

  constructor(
    private readonly cargandoService: CargandoService,
    public _loginService: LoginService,
  ) {

  }

  ngOnInit(): void {
    const datos: any = this._loginService.recuperarDatos();
    this.nombreUsuario = datos.nombre
    this.cargandoService.cambioBloqueado.subscribe(
      (r: any) => {
        if (!this.cargandoService.estaBloqueado) {
          console.log('cuantoas veces entra ');
          this.blockUI.start('Cargando...');
        } else {
          console.log('ya no esta bloqueado');
          this.blockUI.stop();
        }
      }
    );

  }


  logOut() {
    this._loginService.logout();
  }
}
