export interface BodegaInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  habilitado?: boolean;
  esMantenimiento? : boolean;
}
