import {BusquedaGenerica} from '../../../interfaces/busqueda-generica';

export interface BusquedaBodegas extends BusquedaGenerica{
  busqueda?: string;
}
