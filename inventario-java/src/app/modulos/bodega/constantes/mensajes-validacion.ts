export const MENSAJES_VALIDACION_NOMBRE = {
  required: 'El campo nombre es requerido',
  minlength: 'El campo nombre debe tener minimo 3 caracteres',
  maxlength: 'El campo nombre debe tener maximo 10 caracteres',
  pattern: 'Solo se aceptan letras',
}
export const MENSAJES_VALIDACION_STOCK = {
  required: 'El campo nombre es requerido',
  minlength: 'El campo nombre debe tener minimo 3 caracteres',
  maxlength: 'El campo nombre debe tener maximo 10 caracteres',
  pattern: 'Solo se aceptan letras',
}
export const MENSAJES_VALIDACION_CODIGO = {
  required: 'El campo código es requerido',
  minlength: 'El campo código debe tener minimo 3 caracteres',
  maxlength: 'El campo código debe tener maximo 10 caracteres',
}
export const MENSAJES_VALIDACION_CODIGO_AUXILIAR = {
  required: 'El campo código auxiliar es requerido',
  minlength: 'El campo código debe tener minimo 3 caracteres',
  maxlength: 'El campo código debe tener maximo 10 caracteres',
}
export const MENSAJES_VALIDACION_DIRECCION = {
  required: 'El campo dirección es requerido',
  minlength: 'El campo dirección debe tener minimo 3 caracteres',
  maxlength: 'El campo dirección debe tener maximo 10 caracteres',
}
export const MENSAJES_VALIDACION_DESCRIPCION = {
  required: 'El campo descripción es requerido',
  minlength: 'El campo descripción debe tener minimo 3 caracteres',
  maxlength: 'El campo descripción debe tener maximo 10 caracteres',
}
export const MENSAJES_VALIDACION_ES_MANTENIMIENTO = {
  required: 'El campo descripción es requerido',
}
export const MENSAJES_VALIDACION_IMAGENURL = {
  minlength: 'El campo url imagen debe tener minimo 3 caracteres',
  maxlength: 'El campo descripción debe tener maximo 10 caracteres',
}
export const MENSAJES_VALIDACION_USUARIO = {
  required: 'El campo usuario es requerido',
  minlength: 'El campo usuario debe tener minimo 3 caracteres',
  maxlength: 'El campo usuario debe tener maximo 10 caracteres',
}
export const MENSAJES_VALIDACION_CEDULA = {
  required: 'El campo cédula es requerido',
  minlength: 'El campo usuario debe tener minimo 10 caracteres',
  maxlength: 'El campo usuario debe tener maximo 10 caracteres',
  pattern:  'Solo números'
}
export const MENSAJES_VALIDACION_PASSWORD = {
  required: 'El campo password es requerido',
  minlength: 'El campo usuario debe tener minimo 8 caracteres',
}

export const MENSAJES_VALIDACION_TIPO_MANTENIMIENTO = {
  required: 'El campo código es requerido',
}
