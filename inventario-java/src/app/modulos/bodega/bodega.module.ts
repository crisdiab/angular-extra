import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BodegaRoutingModule} from './bodega-routing.module';
import {RutaGestionBodegasComponent} from './rutas/ruta-gestion-bodegas/ruta-gestion-bodegas.component';
import {ModalCrearEditarBodegaComponent} from './componentes/modal-crear-editar-bodega/modal-crear-editar-bodega.component';
import {FormularioBodegaComponent} from './componentes/formulario-bodega/formulario-bodega.component';
import {MODULOS_COMUNES} from '../../constantes/modulos-comunes';
import {RutaInventarioBodegaComponent} from '../inventario/rutas/ruta-inventario-bodega/ruta-inventario-bodega.component';
import {RutaMantenimientoInventarioComponent} from '../inventario/rutas/ruta-mantenimiento-inventario/ruta-mantenimiento-inventario.component';
import {SeleccionarBodegaComponent} from '../inventario/componentes/seleccionar-bodega/seleccionar-bodega.component';
import {SeleccionarDepartamentoComponent} from '../inventario/componentes/seleccionar-departamento/seleccionar-departamento.component';
import {SeleccionarProductoComponent} from '../../componentes/seleccionar-producto/seleccionar-producto.component';
import {ModalAgregarProductoInventarioComponent} from '../inventario/componentes/modal-agregar-producto-inventario/modal-agregar-producto-inventario.component';
import {ModalNuevoMantenimientoComponent} from '../inventario/componentes/modal-nuevo-mantenimiento/modal-nuevo-mantenimiento.component';
import {SeleccionarArticuloInventarioComponent} from '../inventario/componentes/seleccionar-articulo-inventario/seleccionar-articulo-inventario.component';
import {FormularioNuevoMantenimientoComponent} from '../inventario/componentes/formulario-nuevo-mantenimiento/formulario-nuevo-mantenimiento.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatOptionModule} from "@angular/material/core";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {InputTextModule} from "primeng/inputtext";
import {MatSelectModule} from "@angular/material/select";
import { RutaArticuloInventarioComponent } from '../inventario/rutas/ruta-articulo-inventario/ruta-articulo-inventario.component';
import { FormularioBodegaInventarioComponent } from '../inventario/componentes/formulario-bodega-inventario/formulario-bodega-inventario.component';
import { ModalCrearEditarBodegaInventarioComponent } from '../inventario/componentes/modal-crear-editar-bodega-inventario/modal-crear-editar-bodega-inventario.component';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';


@NgModule({
  declarations: [
    RutaGestionBodegasComponent,
    ModalCrearEditarBodegaComponent,
    FormularioBodegaComponent,
  ],
  imports: [
    CommonModule,
    BodegaRoutingModule,
    ...MODULOS_COMUNES,
    ReactiveFormsModule,
    MatOptionModule,
    MatAutocompleteModule,
    InputTextModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatCheckboxModule
  ]
})
export class BodegaModule {
}
