import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaGestionBodegasComponent} from './rutas/ruta-gestion-bodegas/ruta-gestion-bodegas.component';
import {RutaInventarioBodegaComponent} from '../inventario/rutas/ruta-inventario-bodega/ruta-inventario-bodega.component';
import {RutaMantenimientoInventarioComponent} from '../inventario/rutas/ruta-mantenimiento-inventario/ruta-mantenimiento-inventario.component';
import {RutaArticuloInventarioComponent} from "../inventario/rutas/ruta-articulo-inventario/ruta-articulo-inventario.component";

const routes: Routes = [
  {
    path:'gestion',
    component: RutaGestionBodegasComponent
  },
  {
    path: '',
    redirectTo:'gestion'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BodegaRoutingModule { }
