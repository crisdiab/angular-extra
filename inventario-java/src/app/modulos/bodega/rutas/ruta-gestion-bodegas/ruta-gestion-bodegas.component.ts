import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {BodegaInterface} from '../../interfaces/bodega.interface';
import {TABLAS} from '../../../../constantes/tablas';
import {ModalCrearEditarBodegaComponent} from '../../componentes/modal-crear-editar-bodega/modal-crear-editar-bodega.component';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {BodegaRestService} from '../../../../servicios/rest/bodega-rest.service';
import {BusquedaDepartamentos} from '../../../departamento/interfaces/busqueda-departamentos';
import {PAGINACION} from '../../../../constantes/skip-take';
import {BusquedaBodegas} from '../../interfaces/busqueda-bodegas';
import {DepartamentoInterface} from '../../../departamento/interfaces/departamento.interface';
import {encontrarIndice} from '../../../../funciones/encontrar-indice';

@Component({
  selector: 'app-ruta-gestion-bodegas',
  templateUrl: './ruta-gestion-bodegas.component.html',
  styleUrls: ['./ruta-gestion-bodegas.component.scss']
})
export class RutaGestionBodegasComponent implements OnInit {

  columnasPrimeTable = [
    {field: 'nombre', header: 'Nombre', width: '70'},
    {field: 'id', header: 'Acciones', width: '30'}];
  filas = TABLAS.rows;
  bodegas: BodegaInterface[] = [];
  totalBodegas = 0;
  textoBusqueda?: string;

  constructor(
    private readonly _dialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _bodegaRestService: BodegaRestService
  ) {
  }

  ngOnInit(): void {
    this.cargarDatos();
  }

  limpiarFiltroGeneral() {

  }


  cargarDatos(skip?: number, take?: number) {
    this._cargandoService.habilitarCargando();
    let busqueda: BusquedaBodegas = {
      skip: skip ? skip : 0,
      take: take ? take : PAGINACION.take,
      busqueda: ''
    };
    if (this.textoBusqueda !== '' && this.textoBusqueda) {
      busqueda.busqueda = this.textoBusqueda;
    }
    this._bodegaRestService.buscar(busqueda)
      .subscribe(
        (datos: any) => {
          this._cargandoService.deshabilitarCargando();
          this.bodegas = datos.listaObjetos;
          this.totalBodegas = this.textoBusqueda !== '' ? datos.cantidadRegistros : this.bodegas.length;
        },
        error => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.error('Ocurrió un error', 'Error');
        }
      );


  }

  cargarMasDatos(evento: any) {
    this.cargarDatos(evento.first, evento.rows);
  }

  abrirModalCrearEditar(bodega?: BodegaInterface | undefined) {
    const modalCargarArchivo = this._dialog.open(ModalCrearEditarBodegaComponent,
      {
        data: {
          bodega: bodega
        }
      });
    modalCargarArchivo.afterClosed()
      .subscribe(
        (respuesta: any) => {
          this.cargarDatos();
        }
      );

  }

  reiniciarArreglo() {
    this.bodegas = [];
    this.totalBodegas = 0;
  }

  activarDesactivarRegistro(bodega: BodegaInterface) {
    this._cargandoService.habilitarCargando();
    const indice = encontrarIndice(this.bodegas, bodega);
    const departamentoActualizar = {
      habilitado: !bodega.habilitado,
      idBodega: bodega.id
    };
    bodega.habilitado = !bodega.habilitado;
    this._bodegaRestService
      .editar(bodega, bodega.id as number)
      .subscribe(
        r => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.success('Actualizado correctament', 'Correcto');
          this.bodegas[indice].habilitado = bodega.habilitado;
        },
        error => {
          this._toastrService.error('Ocurrió un error', 'Error');
          this._cargandoService.deshabilitarCargando();
        }
      );
  }
}
