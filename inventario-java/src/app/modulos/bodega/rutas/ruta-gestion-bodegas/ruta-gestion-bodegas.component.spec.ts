import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaGestionBodegasComponent } from './ruta-gestion-bodegas.component';

describe('RutaGestionBodegasComponent', () => {
  let component: RutaGestionBodegasComponent;
  let fixture: ComponentFixture<RutaGestionBodegasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RutaGestionBodegasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaGestionBodegasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
