import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioBodegaComponent } from './formulario-bodega.component';

describe('FormularioBodegaComponent', () => {
  let component: FormularioBodegaComponent;
  let fixture: ComponentFixture<FormularioBodegaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioBodegaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioBodegaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
