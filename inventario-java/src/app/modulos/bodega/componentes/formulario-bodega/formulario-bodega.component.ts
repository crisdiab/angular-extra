import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormControlName, FormGroup, Validators} from '@angular/forms';
import {BodegaInterface} from '../../interfaces/bodega.interface';
import {Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {
  MENSAJES_VALIDACION_CODIGO, MENSAJES_VALIDACION_DESCRIPCION,
  MENSAJES_VALIDACION_DIRECCION,
  MENSAJES_VALIDACION_NOMBRE
} from '../../constantes/mensajes-validacion';

@Component({
  selector: 'app-formulario-bodega',
  templateUrl: './formulario-bodega.component.html',
  styleUrls: ['./formulario-bodega.component.scss']
})
export class FormularioBodegaComponent implements OnInit {

  formularioBodega?: FormGroup;
  formularioValido = false;
  @Input() bodega?: BodegaInterface;
  mensajesErrorNombre: string[] = [];
  mensajesErrorDescripcion: string[] = [];
  mensajesErrorEsMantenimiento: string[] = [];
  private _mensajesValidacionNombre = MENSAJES_VALIDACION_NOMBRE;
  private _mensajesValidacionDescripcion = MENSAJES_VALIDACION_DESCRIPCION
  private _mensajesValidacionEsMantenimiento = MENSAJES_VALIDACION_DESCRIPCION

  subscriptores: Subscription[] = [];

  @Output() formularioEmitter: EventEmitter<BodegaInterface | undefined> = new EventEmitter<BodegaInterface | undefined>();

  constructor(private readonly _formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.inciarFormulario();
    this._escucharFormulario();
    this._escucharCampoNombre();
    this._escucharCampoDescripcion();
    this._escucharCampoEsMantenimiento();

  }

  ngOnDestroy(): void {
    this.subscriptores.forEach(valor => valor.unsubscribe());
  }

  inciarFormulario (){
    this.formularioBodega = this._formBuilder.group(
      {
        nombre: [this.bodega ? this.bodega.nombre : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ],
        ],
        descripcion: [this.bodega ? this.bodega.descripcion : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ],
        ],
        esMantenimiento: [this.bodega ? this.bodega.esMantenimiento: '',
          [
            Validators.required,
          ]
        ]
      })
  }

  private _escucharFormulario() {
    // el observable que escucha cambios
   if(this.formularioBodega){
     const observableCambiosFormulario$ = this.formularioBodega.valueChanges
     const observableFOmrulario = observableCambiosFormulario$
       .subscribe((formulario) => {
         if (this.formularioBodega?.valid) {
           this.formularioEmitter.emit(formulario)
         } else {
           this.formularioEmitter.emit(undefined)
         }
       })
     this.subscriptores.push(observableFOmrulario);
   }
  }

  private _escucharCampoNombre() {
    const controlNombre = this.formularioBodega?.get('nombre') as AbstractControl;

    const observableDeCambiosNombre$ = controlNombre.valueChanges;

    const respuestaObservableNombre = observableDeCambiosNombre$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesNombre(controlNombre)
      })
    this.subscriptores.push(respuestaObservableNombre);
  }

  private _setearMensajesNombre(controlForm: AbstractControl) {
    this.mensajesErrorNombre = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorNombre = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' | 'pattern' = atributo as 'required' | 'minlength' | 'maxlength' | 'pattern'
          return this._mensajesValidacionNombre[mensajeError]
        })
    }
  }

  private _escucharCampoDescripcion() {
    const controlDescripcion = this.formularioBodega?.get('descripcion') as AbstractControl;

    const observableDeCambiosDescriocion$ = controlDescripcion.valueChanges;

    const respuestaObservableDescripcion = observableDeCambiosDescriocion$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesDescripcion(controlDescripcion)
      })
    this.subscriptores.push(respuestaObservableDescripcion);
  }

  private _setearMensajesDescripcion(controlForm: AbstractControl) {
    this.mensajesErrorDescripcion = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorDescripcion = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength'  = atributo as 'required' | 'minlength' | 'maxlength'
          return this._mensajesValidacionDescripcion[mensajeError]
        })
    }
  }
  private _escucharCampoEsMantenimiento() {
    const controlDescripcion = this.formularioBodega?.get('esMantenimiento') as AbstractControl;

    const observableDeCambiosEsMantenimiento$ = controlDescripcion.valueChanges;

    const respuestaObservableEsMantenimiento = observableDeCambiosEsMantenimiento$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesEsMantenimiento(controlDescripcion)
      })
    this.subscriptores.push(respuestaObservableEsMantenimiento);
  }

  private _setearMensajesEsMantenimiento(controlForm: AbstractControl) {
    this.mensajesErrorEsMantenimiento = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorEsMantenimiento = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required'  = atributo as 'required'
          return this._mensajesValidacionEsMantenimiento[mensajeError]
        })
    }
  }
}
