import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-seleccionar-fecha',
  templateUrl: './seleccionar-fecha.component.html',
  styleUrls: ['./seleccionar-fecha.component.scss']
})
export class SeleccionarFechaComponent implements OnInit {

  fechaSeleccionada: Date | undefined = undefined;
  fechaInicial = new Date();

  @Output() fechaEmitir: EventEmitter<Date | undefined> = new EventEmitter<Date | undefined>();

  constructor() {
    moment.locale('es');
  }

  ngOnInit(): void {
  }

  emitirSeleccionoFecha(fecha: Date | undefined) {
    if (fecha) {
      this.fechaEmitir.emit(fecha);
    } else {
      this.fechaEmitir.emit(fecha as undefined);
    }

  }
}
