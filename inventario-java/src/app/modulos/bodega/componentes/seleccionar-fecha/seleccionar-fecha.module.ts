import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeleccionarFechaComponent } from './seleccionar-fecha/seleccionar-fecha.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [
    SeleccionarFechaComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatIconModule,
    MatDatepickerModule,
    MatInputModule,
    FormsModule,
    MatButtonModule
  ],
  exports: [
    SeleccionarFechaComponent
  ]
})
export class SeleccionarFechaModule { }
