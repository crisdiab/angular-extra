import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {BodegaInterface} from '../../interfaces/bodega.interface';
import {BodegaRestService} from '../../../../servicios/rest/bodega-rest.service';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-modal-crear-editar-bodega',
  templateUrl: './modal-crear-editar-bodega.component.html',
  styleUrls: ['./modal-crear-editar-bodega.component.scss']
})
export class ModalCrearEditarBodegaComponent implements OnInit {

  bodegaACrear?: BodegaInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { bodega: BodegaInterface | undefined },
    private readonly _bodegaRestService: BodegaRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
  }

  guardarRegistro() {
    this._cargandoService.habilitarCargando();
    if(this.data.bodega){
      // @ts-ignore
      this.bodegaACrear.id = this.data.bodega.id;
      // @ts-ignore
      this.bodegaACrear?.habilitado = this.data.bodega.habilitado;
      this._bodegaRestService
        .editar(this.bodegaACrear as BodegaInterface, this.data.bodega.id as number)
        .subscribe(
          creado => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Se actualizó correctamente');
            this._dialog.closeAll();
          },
          error => {
            console.error({
              error
            });
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        )
    }else {
      // @ts-ignore
      this.bodegaACrear.habilitado = true;
      this._bodegaRestService
        .crear(this.bodegaACrear as BodegaInterface)
        .subscribe(
          creado => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Se creó correctamente');
            this._dialog.closeAll();
          },
          error => {
            console.error({
              error
            });
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        );
    }


  }

  setearBodega(bodega: BodegaInterface | undefined) {
    this.bodegaACrear = bodega;
  }
}
