import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarBodegaComponent } from './modal-crear-editar-bodega.component';

describe('ModalCrearEditarBodegaComponent', () => {
  let component: ModalCrearEditarBodegaComponent;
  let fixture: ComponentFixture<ModalCrearEditarBodegaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarBodegaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarBodegaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
