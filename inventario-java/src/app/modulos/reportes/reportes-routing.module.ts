import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaReporteComponent} from './rutas/ruta-reporte/ruta-reporte.component';

const routes: Routes = [
  {
    path: 'reporte-mantenimiento',
    component: RutaReporteComponent
  },
  {
    path: '',
    redirectTo: 'reporte-mantenimiento'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
