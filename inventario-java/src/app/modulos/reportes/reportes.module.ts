import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { RutaReporteComponent } from './rutas/ruta-reporte/ruta-reporte.component';
import {TituloRutaModule} from '../../componentes/titulo-ruta/titulo-ruta.module';
import {MatCardModule} from '@angular/material/card';
import {MODULOS_COMUNES} from '../../constantes/modulos-comunes';
import {ChartModule} from 'primeng/chart';
import {AccordionModule} from 'primeng/accordion';
import {ButtonModule} from 'primeng/button';
import {MatSelectModule} from '@angular/material/select';
import {SeleccionarProductoInventarioModule} from '../../componentes/seleccionar-producto-inventario/seleccionar-producto-inventario.module';


@NgModule({
  declarations: [RutaReporteComponent],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    TituloRutaModule,
    ChartModule,
    ...MODULOS_COMUNES,
    AccordionModule,
    ButtonModule,
    MatSelectModule,
    SeleccionarProductoInventarioModule
  ]
})
export class ReportesModule { }
