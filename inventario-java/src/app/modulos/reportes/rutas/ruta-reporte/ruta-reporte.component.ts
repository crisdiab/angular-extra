import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';
import {ProductoInterface} from '../../../producto/interfaces/producto.interface';
import {MantenimientoInterface} from '../../../inventario/interfaces/mantenimiento.interface';
import {KardexInterface} from '../../../inventario/interfaces/kardex.interface';
import {ArticuloInventarioRestService} from '../../../../servicios/rest/articulo-inventario-rest.service';
import {MantenimientoRestService} from '../../../../servicios/rest/mantenimiento-rest.service';
import {CargandoService} from '../../../../servicios/cargando.service';

@Component({
  selector: 'app-ruta-reporte',
  templateUrl: './ruta-reporte.component.html',
  styleUrls: ['./ruta-reporte.component.scss']
})
export class RutaReporteComponent implements OnInit {

  activeState: boolean[] = [true, false, false];
  datosPorEstado: any;
  datosPorProducto: any;
  datosPorUsuario: any;
  datosPorInventarioProducto: any;
  datosPorInventarioGeneral: any;
  porMantenimiento = false;
  estadoSeleccionado: any;
  rangeDates?: Date[];
  fechaInicial?: string;
  fechaFinal?: string;
  productoSeleccionado?: ProductoInterface;
  usuarioSeleccionado?: string = '';
  mantenimientosEncontrados?: MantenimientoInterface[];
  productosKardexEncontrados?: KardexInterface[];

  constructor(
    private readonly _toastrService: ToastrService,
    private readonly _articuloInventarioService: ArticuloInventarioRestService,
    private readonly _mantenimientoRestService: MantenimientoRestService,
    private readonly _cargandoService: CargandoService
  ) {
    moment.locale('es');
  }

  generarDatosPorEstado() {
    // 'PR' | 'ER' | 'PE' | 'EN' | 'NR';
    // PR= 'Por recibir' ,
    //   ER= 'En revisión'  ,
    //   PE= 'Por entregar'  ,
    //   EN= 'Entregado',
    //   NR = 'No realizado'
    if (this.mantenimientosEncontrados) {
      this.datosPorEstado = {
        labels: ['Por recibir', 'En revision', 'Por entregar', 'Entregado', 'No realizado',],
        datasets: [
          {
            label: 'Estados',
            data: [
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'PR'),
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'ER'),
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'PE'),
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'EN'),
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'NR'),
            ]
          },
        ]
      };
    }

  }

  // por mantenimiento

  buscarMantenimientoReporteMantenimientoEstado() {
    console.log('range dates', this.rangeDates)
    // @ts-ignore
    this.fechaInicial = moment(this.rangeDates[0]).format('DD-MM-yyyy');
    // @ts-ignore
    this.fechaFinal = moment(this.rangeDates[1]).format('DD-MM-yyyy');
    const busqueda: ReporteConsultaInterface = {
      fechaInicio : this.fechaInicial,
      fechaFin: this.fechaFinal,
    }
    this._cargandoService.habilitarCargando();
    this._mantenimientoRestService
      .generarReporteEstado(busqueda)
      .subscribe(
        (r: any) => {
          this.mantenimientosEncontrados = r;
          this.generarDatosPorEstado();
        },
        error => {
          this._cargandoService.deshabilitarCargando();
          console.error(error);
        }
      );
  }

  buscarMantenimientoReporteMantenimientoUsuario() {
    // @ts-ignore
    this.fechaInicial = moment(this.rangeDates[0]).format('DD-MM-yyyy');
    // @ts-ignore
    this.fechaFinal = moment(this.rangeDates[1]).format('DD-MM-yyyy');

    const busqueda: ReporteConsultaInterface = {
      fechaInicio : this.fechaInicial,
      fechaFin: this.fechaFinal,
      cedulaTecnico : this.usuarioSeleccionado
    }
    this._cargandoService.habilitarCargando();
    this._mantenimientoRestService
      .generarReporteCedula(busqueda)
      .subscribe(
        (r: any) => {
          this.mantenimientosEncontrados = r;
          console.log(r)
          if(this.mantenimientosEncontrados){
            console.log('entro')
            const operarios: string[] = this.obtenerUsuarios(this.mantenimientosEncontrados)
            console.log('operarios', operarios)
            this.generarDatosPorUsuario()
          }

        },
        error => {
          this._cargandoService.deshabilitarCargando();
          console.error(error);
        }
      );
  }

  buscarMantenimientoReporteMantenimientoArticuloInventario() {
    if(this.productoSeleccionado && this.productoSeleccionado.id){
      // @ts-ignore
      this.fechaInicial = moment(this.rangeDates[0]).format('DD-MM-yyyy');
      // @ts-ignore
      this.fechaFinal = moment(this.rangeDates[1]).format('DD-MM-yyyy');
      const busqueda: ReporteConsultaInterface = {
        fechaInicio : this.fechaInicial,
        fechaFin: this.fechaFinal,
        articuloInvId : (this.productoSeleccionado.id).toString()
      }
      this._cargandoService.habilitarCargando();
      this._mantenimientoRestService
        .generarReporteArticuloInventario(busqueda)
        .subscribe(
          (r: any) => {
            this.mantenimientosEncontrados = r;
            this.generarDatosPorProducto();
          },
          error => {
            this._cargandoService.deshabilitarCargando();
            console.error(error);
          }
        );
    }

  }


  // por kardex

  buscarIngresosEgresosPorArticulo(){

    this._cargandoService.habilitarCargando();
    // @ts-ignore
    this.fechaInicial = moment(this.rangeDates[0]).format('DD-MM-yyyy');
    // @ts-ignore
    this.fechaFinal = moment(this.rangeDates[1]).format('DD-MM-yyyy');
    const busqueda: ReporteConsultaInterface = {
      busqueda: this.productoSeleccionado?.id + '',
      fechaInicio: this.fechaInicial,
      fechaFin: this.fechaFinal
    }
    this._articuloInventarioService
      .generarReporteIngresosEgresos(busqueda)
      .subscribe(
        (r: any)=> {
          this._cargandoService.deshabilitarCargando();
          this.productosKardexEncontrados = r.listaObjetos;
          this.generarDatosPorInventarioProducto()
        }
      );
  }
  buscarIngresosEgresosGeneral(){
    // @ts-ignore
    this.fechaInicial = moment(this.rangeDates[0]).format('DD-MM-yyyy');
    // @ts-ignore
    this.fechaFinal = moment(this.rangeDates[1]).format('DD-MM-yyyy');
    this._cargandoService.habilitarCargando();
    const busqueda: ReporteConsultaInterface = {
      fechaInicio: this.fechaInicial,
      fechaFin: this.fechaFinal,
      busqueda: ''
    }
    this._articuloInventarioService
      .generarReporteIngresosEgresos(busqueda)
      .subscribe(
        (r: any)=> {
          this._cargandoService.deshabilitarCargando();
          this.productosKardexEncontrados = r.listaObjetos;
          this.generarDatosPorInventarioGeneral()
        }
      );
  }


  filtrarPorEstado(arreglo: MantenimientoInterface[], estado: 'PR' | 'ER' | 'PE' | 'EN' | 'NR') {
    return arreglo.filter((elemento) => elemento.estado === estado).length;
  }

  generarDatosPorProducto() {
    if (this.mantenimientosEncontrados) {
      this.datosPorProducto = {
        labels: ['Por recibir', 'En revision', 'Por entregar', 'Entregado', 'No realizado',],
        datasets: [
          {
            label: 'Estados',
            data: [
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'PR'),
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'ER'),
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'PE'),
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'EN'),
              this.filtrarPorEstado(this.mantenimientosEncontrados, 'NR'),
            ]
          },
        ]
      };
    }

  }

  filtrarPorTipoKardex(arregloKardex: KardexInterface[], estado: boolean) {
    return arregloKardex.filter((elemento) => elemento.suma === estado).length;
  }


  generarDatosPorUsuario() {
    if (this.mantenimientosEncontrados) {
      const usuarios: any = this.obtenerUsuarios(this.mantenimientosEncontrados);
      this.datosPorUsuario = {
        labels: usuarios,
        datasets: [
          {
            label: 'Operarios',
            data: this.generarDataOperarios(this.mantenimientosEncontrados, usuarios),
          },
        ]
      };
    }

  }

  obtenerUsuarios(arregloMantenimientos: MantenimientoInterface[]): any {
    const nombres = arregloMantenimientos.map(elemento => elemento.nombreTecnico);
    const eliminarRepetidos = new Set(nombres);
    return [...eliminarRepetidos];
  }

  filtarPorNombre(arregloNombres: MantenimientoInterface[], nombre: string) {
    return arregloNombres.filter(elemento => elemento.nombreTecnico === nombre).length;
  }

  generarDataOperarios(arregloMantenimientos: MantenimientoInterface[], arregloNombres: string[]) {
    const datos: number[] = [];
    arregloNombres.forEach(
      (elemento, indice) => {
        const total = this.filtarPorNombre(arregloMantenimientos, elemento);
        datos.push(total);
      }
    );
    return datos;
  }

  generarDatosPorInventarioProducto() {
    if (this.productosKardexEncontrados) {
      console.log(
        this.filtrarPorTipoKardex(this.productosKardexEncontrados, true),
        this.filtrarPorTipoKardex(this.productosKardexEncontrados, false),
      );
      this.datosPorInventarioProducto = {
        labels: ['Ingreso', 'Egreso'],
        datasets: [
          {
            label: 'Producto',
            data: [
              this.filtrarPorTipoKardex(this.productosKardexEncontrados, true),
              this.filtrarPorTipoKardex(this.productosKardexEncontrados, false),
            ]
          },
        ]
      };
    }
  }

  generarDatosPorInventarioGeneral() {

    if (this.productosKardexEncontrados) {
      this.datosPorInventarioGeneral = {
        labels: ['Ingreso', 'Egreso',],
        datasets: [
          {
            label: 'Productos',
            data: [
              this.filtrarPorTipoKardex(this.productosKardexEncontrados, true),
              this.filtrarPorTipoKardex(this.productosKardexEncontrados, false),
            ]
          },

        ]
      };
    }

  }

  ngOnInit(): void {
  }

  toggle(index: number) {
    this.activeState[index] = !this.activeState[index];
  }

  convertirFechas() {
    if (this.rangeDates?.length === 2) {
      const fechaInicio = moment(this.rangeDates[0]);
      const fechaFin = moment(this.rangeDates[1]);
      const cumpleRango30Dias = fechaFin.diff(fechaInicio, 'days') < 31;

      if (cumpleRango30Dias) {

      } else {
        this._toastrService.warning('Rango mayor a 31 dias', 'Cuidado');
      }

    }
  }
}


export interface ReporteConsultaInterface {
  estado?: string;
  fechaInicio?: string;
  fechaFin?: string;
  cedulaTecnico?: string;
  articuloInvId?: string;
  bodegaInvId?: string;
  busqueda?: string;

}
