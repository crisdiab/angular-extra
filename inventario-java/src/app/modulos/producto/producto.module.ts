import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductoRoutingModule } from './producto-routing.module';
import { RutaGestionProductosComponent } from './rutas/ruta-gestion-productos/ruta-gestion-productos.component';
import { ModalCrearEditarProductoComponent } from './componentes/modal-crear-editar-producto/modal-crear-editar-producto.component';
import { FormularioProductoComponent } from './componentes/formulario-producto/formulario-producto.component';
import {MatCardModule} from '@angular/material/card';
import {TituloRutaModule} from '../../componentes/titulo-ruta/titulo-ruta.module';
import {MODULOS_COMUNES} from '../../constantes/modulos-comunes';
import {ReactiveFormsModule} from '@angular/forms';
import { DetallesAdicionalesComponent } from './componentes/detalles-adicionales/detalles-adicionales.component';


@NgModule({
  declarations: [RutaGestionProductosComponent, ModalCrearEditarProductoComponent, FormularioProductoComponent, DetallesAdicionalesComponent],
    imports: [
        CommonModule,
        ProductoRoutingModule,
        ...MODULOS_COMUNES,
        ReactiveFormsModule
    ]
})
export class ProductoModule { }
