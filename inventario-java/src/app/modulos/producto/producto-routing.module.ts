import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaGestionProductosComponent} from './rutas/ruta-gestion-productos/ruta-gestion-productos.component';

const routes: Routes = [
  {
    path: 'gestion',
    component: RutaGestionProductosComponent
  },
  {
    path: '',
    redirectTo: 'gestion'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductoRoutingModule { }
