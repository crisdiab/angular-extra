import {Component, OnInit} from '@angular/core';
import {TABLAS} from '../../../../constantes/tablas';
import {BodegaInterface} from '../../../bodega/interfaces/bodega.interface';
import {MatDialog} from '@angular/material/dialog';
import {ProductoInterface} from '../../interfaces/producto.interface';
import {ModalCrearEditarProductoComponent} from '../../componentes/modal-crear-editar-producto/modal-crear-editar-producto.component';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {ProductoRestService} from '../../../../servicios/rest/producto-rest.service';
import {BusquedaDepartamentos} from '../../../departamento/interfaces/busqueda-departamentos';
import {PAGINACION} from '../../../../constantes/skip-take';
import {DepartamentoInterface} from '../../../departamento/interfaces/departamento.interface';
import {encontrarIndice} from '../../../../funciones/encontrar-indice';

@Component({
  selector: 'app-ruta-gestion-productos',
  templateUrl: './ruta-gestion-productos.component.html',
  styleUrls: ['./ruta-gestion-productos.component.scss']
})
export class RutaGestionProductosComponent implements OnInit {

  columnasPrimeTable = [
    {field: 'nombre', header: 'Nombre', width: '70'},
    {field: 'id', header: 'Acciones', width: '30'}];
  filas = TABLAS.rows;
  productos: ProductoInterface[] = [
  ];
  totalProductos = 0;
  textoBusqueda?: string;

  constructor(
    private readonly _dialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _productoRestService: ProductoRestService
  ) {
  }

  ngOnInit(): void {
  }

  editar(bodega: BodegaInterface) {

  }

  limpiarFiltroGeneral() {

  }


  cargarDatos(skip?: number, take?: number) {
    this._cargandoService.habilitarCargando();
    let busqueda: BusquedaDepartamentos = {
      skip: skip ? skip : 0,
      take: take ? take : PAGINACION.take,
      busqueda: ''
    };
    if(this.textoBusqueda !== '' && this.textoBusqueda) {
      busqueda.busqueda = this.textoBusqueda
    }
    this._productoRestService.buscar(busqueda)
      .subscribe(
        (datos: any) => {
          this._cargandoService.deshabilitarCargando();
          this.productos = datos.listaObjetos;
          this.totalProductos = this.textoBusqueda !== '' ? datos.cantidadRegistros: this.productos.length ;
        },
        error => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.error('Ocurrió un error', 'Error');
        }
      );
  }

  cargarMasDatos(evento: any) {
    this.cargarDatos(evento.first, evento.rows);
  }

  abrirModalCrearEditar(producto?: ProductoInterface | undefined) {
    const modalCargarArchivo = this._dialog.open(ModalCrearEditarProductoComponent,
      {
        data: {
          producto: producto
        }
      });
    modalCargarArchivo.afterClosed()
      .subscribe(
        (r) => this.cargarDatos()
      )
  }

  reiniciarArreglo() {
    this.productos = [];
    this.totalProductos = 0;
  }

  activarDesactivarRegistro(producto: ProductoInterface) {
    this._cargandoService.habilitarCargando();
    const indice = encontrarIndice(this.productos, producto);
    const productoActualizar = {
      habilitado : !producto.habilitado,
      idProducto: producto.id
    }
    producto.habilitado = !producto.habilitado
    this._productoRestService
      .editar(producto, producto.id as number)
      .subscribe(
        r => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.success('Actualizado correctament', 'Correcto');
          this.productos[indice].habilitado = !producto.habilitado;
        },
        error => {
          this._toastrService.error('Ocurrió un error', 'Error')
          this._cargandoService.deshabilitarCargando();
          console.error(error)
        }
      )
  }
}
