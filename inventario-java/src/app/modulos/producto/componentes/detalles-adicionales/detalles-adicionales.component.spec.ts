import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesAdicionalesComponent } from './detalles-adicionales.component';

describe('DetallesAdicionalesComponent', () => {
  let component: DetallesAdicionalesComponent;
  let fixture: ComponentFixture<DetallesAdicionalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallesAdicionalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesAdicionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
