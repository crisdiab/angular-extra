import {Component, Inject, OnInit} from '@angular/core';
import {DepartamentoInterface} from '../../../departamento/interfaces/departamento.interface';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {DepartamentoRestService} from '../../../../servicios/rest/departamento-rest.service';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {ProductoInterface} from '../../interfaces/producto.interface';
import {ProductoRestService} from '../../../../servicios/rest/producto-rest.service';
import {BodegaInterface} from '../../../bodega/interfaces/bodega.interface';

@Component({
  selector: 'app-modal-crear-editar-producto',
  templateUrl: './modal-crear-editar-producto.component.html',
  styleUrls: ['./modal-crear-editar-producto.component.scss']
})
export class ModalCrearEditarProductoComponent implements OnInit {

  productoACrear?: ProductoInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { producto: ProductoInterface | undefined },
    private readonly _productoRestService: ProductoRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
  }

  guardarRegistro() {
    this._cargandoService.habilitarCargando();
    if (this.data.producto) {
      // @ts-ignore
      this.productoACrear.id = this.data.producto.id;
      // @ts-ignore
      this.productoACrear?.habilitado = this.data.producto.habilitado;
      this._productoRestService
        .editar(this.productoACrear as ProductoInterface, this.data.producto.id as number)
        .subscribe(
          creado => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Se actualizó correctamente');
            this._dialog.closeAll();
          },
          error => {
            console.error({
              error
            });
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        );
    } else {
      if (this.productoACrear) {
        this.productoACrear.habilitado = true;
        this._productoRestService
          .crear(this.productoACrear as BodegaInterface)
          .subscribe(
            creado => {
              this._cargandoService.deshabilitarCargando();
              this._toastrService.success('Se creó correctamente');
              this._dialog.closeAll();
            },
            error => {
              console.error({
                error
              });
              this._cargandoService.deshabilitarCargando();
              this._toastrService.error('Ocurrió un error', 'Error');
            }
          );
      }
    }
  }

  setearProducto(producto: ProductoInterface | undefined) {
    this.productoACrear = producto;
  }

}
