import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarProductoComponent } from './modal-crear-editar-producto.component';

describe('ModalCrearEditarProductoComponent', () => {
  let component: ModalCrearEditarProductoComponent;
  let fixture: ComponentFixture<ModalCrearEditarProductoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarProductoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
