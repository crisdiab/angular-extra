import {DetallesAdicionalesProductoInterface} from './detalles-adicionales-producto.interface';

export interface ProductoInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  codigo?: string;
  codigoAuxiliar?: string;
  imagenUrl?: string;
  habilitado?: boolean;
  detalleAdicional?: DetallesAdicionalesProductoInterface[];
}
