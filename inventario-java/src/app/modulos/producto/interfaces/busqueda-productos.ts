import {BusquedaGenerica} from '../../../interfaces/busqueda-generica';

export interface BusquedaProductos extends BusquedaGenerica {
  busqueda?: string;
}
