export interface DetallesAdicionalesProductoInterface {
  id?: number;
  llave?: string;
  valor?: string;
}
