import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarDepartamentoComponent } from './modal-crear-editar-departamento.component';

describe('ModalCrearEditarDepartamentoComponent', () => {
  let component: ModalCrearEditarDepartamentoComponent;
  let fixture: ComponentFixture<ModalCrearEditarDepartamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarDepartamentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarDepartamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
