import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {DepartamentoInterface} from '../../interfaces/departamento.interface';
import {DepartamentoRestService} from '../../../../servicios/rest/departamento-rest.service';
import {BodegaInterface} from '../../../bodega/interfaces/bodega.interface';

@Component({
  selector: 'app-modal-crear-editar-departamento',
  templateUrl: './modal-crear-editar-departamento.component.html',
  styleUrls: ['./modal-crear-editar-departamento.component.scss']
})
export class ModalCrearEditarDepartamentoComponent implements OnInit {

  departamentoACrear?: DepartamentoInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { departamento: DepartamentoInterface | undefined },
    private readonly _departamentoRestService: DepartamentoRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
  }

  guardarRegistro() {
    this._cargandoService.habilitarCargando();
    if (this.data.departamento) {
      // @ts-ignore
      this.departamentoACrear.id = this.data.departamento.id
      // @ts-ignore
      this.departamentoACrear?.habilitado = this.data.departamento.habilitado;
      this._departamentoRestService
        .editar(this.departamentoACrear as DepartamentoInterface, this.data.departamento.id as number)
        .subscribe(
          creado => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Se actualizó correctamente');
            this._dialog.closeAll();
          },
          error => {
            console.error({
              error
            });
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        )
    } else {
      if(this.departamentoACrear) {
        this.departamentoACrear.habilitado = true;
        this._departamentoRestService
          .crear(this.departamentoACrear as DepartamentoInterface)
          .subscribe(
            creado => {
              this._cargandoService.deshabilitarCargando();
              this._toastrService.success('Se creó correctamente');
              this._dialog.closeAll();
            },
            error => {
              console.error({
                error
              });
              this._cargandoService.deshabilitarCargando();
              this._toastrService.error('Ocurrió un error', 'Error');
            }
          );
      }

    }
  }

  setearDepartamento(departamento: DepartamentoInterface | undefined) {
    this.departamentoACrear = departamento;
  }

}
