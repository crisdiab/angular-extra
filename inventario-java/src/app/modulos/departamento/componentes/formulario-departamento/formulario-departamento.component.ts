import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BodegaInterface} from '../../../bodega/interfaces/bodega.interface';
import {
  MENSAJES_VALIDACION_CODIGO, MENSAJES_VALIDACION_DESCRIPCION,
  MENSAJES_VALIDACION_DIRECCION,
  MENSAJES_VALIDACION_NOMBRE
} from '../../../bodega/constantes/mensajes-validacion';
import {Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {DepartamentoInterface} from '../../interfaces/departamento.interface';

@Component({
  selector: 'app-formulario-departamento',
  templateUrl: './formulario-departamento.component.html',
  styleUrls: ['./formulario-departamento.component.scss']
})
export class FormularioDepartamentoComponent implements OnInit {

  formularioDepartamento?: FormGroup;
  formularioValido = false;
  @Input() departamento?: DepartamentoInterface;
  mensajesErrorNombre: string[] = [];
  mensajesErrorCodigo: string[] = [];
  mensajesErrorDescripcion: string[] = [];
  private _mensajesValidacionNombre = MENSAJES_VALIDACION_NOMBRE;
  private _mensajesValidacionCodigo = MENSAJES_VALIDACION_CODIGO
  private _mensajesValidacionDescripcion = MENSAJES_VALIDACION_DESCRIPCION

  subscriptores: Subscription[] = [];

  @Output() formularioEmitter: EventEmitter<DepartamentoInterface | undefined> = new EventEmitter<DepartamentoInterface | undefined>();

  constructor(private readonly _formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.inicializarFormulario();
    this._escucharFormulario();
    this._escucharCampoNombre();
    this._escucharCampoCodigo();
    this._escucharCampoDescripcion();

  }

  inicializarFormulario(){
    this.formularioDepartamento = this._formBuilder.group(
      {
        nombre: [this.departamento ? this.departamento.nombre : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(60),
            Validators.pattern(/[A-Za-z]+/g),
          ],
        ],
        codigo: [this.departamento ? this.departamento.codigo : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        descripcion: [this.departamento ? this.departamento.descripcion : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(500),
          ],
        ],
      })
  }
  ngOnDestroy(): void {
    this.subscriptores.forEach(valor => valor.unsubscribe());
  }


  private _escucharFormulario() {
    // el observable que escucha cambios
    if(this.formularioDepartamento){
      const observableCambiosFormulario$ = this.formularioDepartamento.valueChanges
      const observableFOmrulario = observableCambiosFormulario$
        .subscribe((formulario) => {
          // console.log('escuchando cambios formulario', formulario)
          if (this.formularioDepartamento?.valid) {
            this.formularioEmitter.emit(formulario)
          } else {
            this.formularioEmitter.emit(undefined)
          }
        })
      this.subscriptores.push(observableFOmrulario);
    }

  }

  private _escucharCampoNombre() {
    const controlNombre = this.formularioDepartamento?.get('nombre') as AbstractControl;

    const observableDeCambiosNombre$ = controlNombre.valueChanges;

    const respuestaObservableNombre = observableDeCambiosNombre$
      .subscribe((respuesta) => {
        this._setearMensajesNombre(controlNombre)
      })
    this.subscriptores.push(respuestaObservableNombre);
  }

  private _setearMensajesNombre(controlForm: AbstractControl) {
    this.mensajesErrorNombre = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorNombre = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' | 'pattern' = atributo as 'required' | 'minlength' | 'maxlength' | 'pattern'
          return this._mensajesValidacionNombre[mensajeError]
        })
    }
  }

  private _escucharCampoCodigo() {
    const controlCodigo = this.formularioDepartamento?.get('codigo') as AbstractControl;

    const observableDeCambiosCodigo$ = controlCodigo.valueChanges;

    const respuestaObservableCodigo = observableDeCambiosCodigo$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesCodigo(controlCodigo)
      })
    this.subscriptores.push(respuestaObservableCodigo);
  }

  private _setearMensajesCodigo(controlForm: AbstractControl) {
    this.mensajesErrorCodigo = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorCodigo = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength'  = atributo as 'required' | 'minlength' | 'maxlength'
          return this._mensajesValidacionCodigo[mensajeError]
        })
    }
  }

  private _escucharCampoDescripcion() {
    const controlDescripcion = this.formularioDepartamento?.get('descripcion') as AbstractControl;

    const observableDeCambiosDescriocion$ = controlDescripcion.valueChanges;

    const respuestaObservableDescripcion = observableDeCambiosDescriocion$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesDescripcion(controlDescripcion)
      })
    this.subscriptores.push(respuestaObservableDescripcion);
  }

  private _setearMensajesDescripcion(controlForm: AbstractControl) {
    this.mensajesErrorDescripcion = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorDescripcion = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength'  = atributo as 'required' | 'minlength' | 'maxlength'
          return this._mensajesValidacionDescripcion[mensajeError]
        })
    }
  }

}
