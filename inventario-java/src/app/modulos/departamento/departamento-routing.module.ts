import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaGestionDepartamentoComponent} from './rutas/ruta-gestion-departamento/ruta-gestion-departamento.component';

const routes: Routes = [
  {
    path: 'gestion',
    component: RutaGestionDepartamentoComponent
  },
  {
    path: '',
    redirectTo: 'gestion'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartamentoRoutingModule { }
