import {BusquedaGenerica} from '../../../interfaces/busqueda-generica';

export interface BusquedaDepartamentos extends BusquedaGenerica {
  busqueda?: string;
  idDepartamento?: number
}
