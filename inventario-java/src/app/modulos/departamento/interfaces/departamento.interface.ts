export interface DepartamentoInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  codigo?: string;
  habilitado?: boolean;
}
