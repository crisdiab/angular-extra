import {BusquedaGenerica} from '../../../interfaces/busqueda-generica';

export interface BusquedaUsuarioInterface extends BusquedaGenerica {
  busqueda?: string;
}
