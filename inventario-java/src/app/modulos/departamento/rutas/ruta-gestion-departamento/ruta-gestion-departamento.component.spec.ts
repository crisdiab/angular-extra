import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaGestionDepartamentoComponent } from './ruta-gestion-departamento.component';

describe('RutaGestionDepartamentoComponent', () => {
  let component: RutaGestionDepartamentoComponent;
  let fixture: ComponentFixture<RutaGestionDepartamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RutaGestionDepartamentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaGestionDepartamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
