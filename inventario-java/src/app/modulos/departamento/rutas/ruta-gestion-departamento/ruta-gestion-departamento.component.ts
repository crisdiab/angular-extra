import {Component, OnInit} from '@angular/core';
import {DepartamentoInterface} from '../../interfaces/departamento.interface';
import {ModalCrearEditarDepartamentoComponent} from '../../componentes/modal-crear-editar-departamento/modal-crear-editar-departamento.component';
import {MatDialog} from '@angular/material/dialog';
import {TABLAS} from '../../../../constantes/tablas';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {DepartamentoRestService} from '../../../../servicios/rest/departamento-rest.service';
import {Router} from '@angular/router';
import {encontrarIndice} from '../../../../funciones/encontrar-indice';
import {BusquedaDepartamentos} from '../../interfaces/busqueda-departamentos';
import {PAGINACION} from '../../../../constantes/skip-take';

@Component({
  selector: 'app-ruta-gestion-departamento',
  templateUrl: './ruta-gestion-departamento.component.html',
  styleUrls: ['./ruta-gestion-departamento.component.scss']
})
export class RutaGestionDepartamentoComponent implements OnInit {

  columnasPrimeTable = [
    {field: 'nombre', header: 'Nombre', width: '70'},
    {field: 'id', header: 'Acciones', width: '30'}];

  departamentos: DepartamentoInterface[] = [

  ];
  filas = TABLAS.rows;
  totalDepartamentos = 0;
  textoBusqueda?: string;

  constructor(
    private readonly _dialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _departamentoRestService: DepartamentoRestService,
    private readonly _router: Router,
  ) {
  }

  ngOnInit(): void {
    this.cargarDatos();
  }


  limpiarFiltroGeneral() {

  }


  cargarDatos(skip?: number, take?: number) {
    this._cargandoService.habilitarCargando();
    let busqueda: BusquedaDepartamentos = {
      skip: skip ? skip : 0,
      take: take ? take : PAGINACION.take,
      busqueda: ''
    };
    if(this.textoBusqueda !== '' && this.textoBusqueda) {
      busqueda.busqueda = this.textoBusqueda
    }
      this._departamentoRestService.buscar(busqueda)
        .subscribe(
          (datos: any) => {
            this._cargandoService.deshabilitarCargando();
            this.departamentos = datos.listaObjetos;
            this.totalDepartamentos = this.textoBusqueda !== '' ? datos.cantidadRegistros :this.departamentos.length;
          },
          error => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        );


  }

  cargarMasDatos(evento: any) {
    this.cargarDatos(evento.first, evento.rows);
  }

  abrirModalCrearEditar(departamento?: DepartamentoInterface | undefined) {
    const modalCargarArchivo = this._dialog.open(ModalCrearEditarDepartamentoComponent,
      {
        data: {
          departamento: departamento
        }
      });
    modalCargarArchivo.afterClosed()
      .subscribe(
        (r) => this.cargarDatos()
      )
  }

  navegarInventario(departamento: DepartamentoInterface) {
    this._router.navigate(['/inventarios',departamento.id, 'bodega-inventario']);

  }

  activarDesactivarRegistro(departamento: DepartamentoInterface) {
    this._cargandoService.habilitarCargando();
    const indice = encontrarIndice(this.departamentos, departamento);
    const departamentoActualizar = {
      habilitado : !departamento.habilitado,
      idDepartamento: departamento.id
    }
    departamento.habilitado = !departamento.habilitado;
    this._departamentoRestService
      .editar(departamento, departamento.id as number)
      .subscribe(
        r => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.success('Actualizado correctament', 'Correcto');
          this.departamentos[indice].habilitado = departamento.habilitado;
        },
        error => {
          this._toastrService.error('Ocurrió un error', 'Error')
          this._cargandoService.deshabilitarCargando();
        }
      )
  }

  reiniciarArreglo() {
    this.departamentos = [];
    this.totalDepartamentos = 0;
  }


}
