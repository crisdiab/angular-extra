import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DepartamentoRoutingModule} from './departamento-routing.module';
import {RutaGestionDepartamentoComponent} from './rutas/ruta-gestion-departamento/ruta-gestion-departamento.component';
import {ModalCrearEditarDepartamentoComponent} from './componentes/modal-crear-editar-departamento/modal-crear-editar-departamento.component';
import {FormularioDepartamentoComponent} from './componentes/formulario-departamento/formulario-departamento.component';
import {MatCardModule} from '@angular/material/card';
import {TituloRutaModule} from '../../componentes/titulo-ruta/titulo-ruta.module';
import {TableModule} from 'primeng/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';
import {ToolbarModule} from 'primeng/toolbar';
import {MatDialogModule} from '@angular/material/dialog';
import {MODULOS_COMUNES} from '../../constantes/modulos-comunes';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [RutaGestionDepartamentoComponent, ModalCrearEditarDepartamentoComponent, FormularioDepartamentoComponent],
    imports: [
        CommonModule,
        DepartamentoRoutingModule,
        ...MODULOS_COMUNES,

    ]
})
export class DepartamentoModule {
}
