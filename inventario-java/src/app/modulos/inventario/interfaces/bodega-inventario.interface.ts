import {ProductoInterface} from '../../producto/interfaces/producto.interface';
import {DepartamentoInterface} from '../../departamento/interfaces/departamento.interface';
import {BodegaInterface} from '../../bodega/interfaces/bodega.interface';

export interface BodegaInventarioInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  codigo?: string;
  direccion?: string;
  habilitado?: boolean;
  departamento?: DepartamentoInterface | number;
  tipoBodega?: BodegaInterface | number;
  esMantenimiento?: boolean;
}
