import {ArticuloInventarioInterface} from "./articulo-inventario.interface";
import {BodegaInventarioInterface} from "./bodega-inventario.interface";

export interface MantenimientoInterface {
  id?: number;
  nombreTecnico?: string;
  cedulaTecnico?: string;
  descripcion?: string;
  nombre?:string;
  estado?: 'PR' | 'ER' | 'PE' | 'EN' | 'NR';
  fechaIngresa?: string;
  fechaIniciaMantenimiento?: string;
  fechaTerminaMantenimiento?: string;
  tipoMantenimiento?: string;
  fechaEntrega?: string;
  articuloInventario?: number | ArticuloInventarioInterface,
}
