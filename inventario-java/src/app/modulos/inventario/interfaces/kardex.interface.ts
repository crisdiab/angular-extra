import {ArticuloInventarioInterface} from './articulo-inventario.interface';

export interface KardexInterface {
  id?: Number;
  suma?: boolean;
  cantidad?: number;
  articuloInventario?: ArticuloInventarioInterface;
  fechaCreacion?: string
}
