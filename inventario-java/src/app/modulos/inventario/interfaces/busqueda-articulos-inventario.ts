import {BusquedaGenerica} from '../../../interfaces/busqueda-generica';

export interface BusquedaArticulosInventario extends BusquedaGenerica{
  busqueda?:string;
  idBodegaInventario?: number;
}
