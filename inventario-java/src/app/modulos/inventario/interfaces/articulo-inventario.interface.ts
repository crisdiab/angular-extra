import {BodegaInventarioInterface} from "./bodega-inventario.interface";
import {ProductoInterface} from "../../producto/interfaces/producto.interface";
import {KardexInterface} from './kardex.interface';
import {MantenimientoInterface} from './mantenimiento.interface';

export interface ArticuloInventarioInterface {
  id?: number;
  bodegaInventario?: BodegaInventarioInterface;
  producto?:  ProductoInterface ;
  stock?: number;
  nombreProducto?: string;
  nombre?: string;
  descripcion?: string;
  codigo?: string;
  codigoAuxiliar?: string;
  habilitado?: boolean;
  kardexs?: KardexInterface[],
  mantenimientos?: MantenimientoInterface[];
}
