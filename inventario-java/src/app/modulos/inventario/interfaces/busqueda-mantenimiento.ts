import {BusquedaGenerica} from '../../../interfaces/busqueda-generica';

export interface BusquedaMantenimiento extends BusquedaGenerica {
  busqueda?: string;
  estado?: 'PR'|'ER'|'PE'|'EN';
  articuloInvId?: number;
}
