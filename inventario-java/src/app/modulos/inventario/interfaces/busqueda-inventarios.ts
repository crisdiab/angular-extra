import {BusquedaGenerica} from '../../../interfaces/busqueda-generica';

export interface BusquedaInventarios extends BusquedaGenerica{
  nombre: string;
}
