import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InventarioRoutingModule} from "./inventario-routing.module";
import {MODULOS_COMUNES} from "../../constantes/modulos-comunes";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatOptionModule} from "@angular/material/core";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {InputTextModule} from "primeng/inputtext";
import {MatSelectModule} from "@angular/material/select";
import {RutaInventarioBodegaComponent} from "./rutas/ruta-inventario-bodega/ruta-inventario-bodega.component";
import {RutaMantenimientoInventarioComponent} from "./rutas/ruta-mantenimiento-inventario/ruta-mantenimiento-inventario.component";
import {SeleccionarBodegaComponent} from "./componentes/seleccionar-bodega/seleccionar-bodega.component";
import {SeleccionarDepartamentoComponent} from "./componentes/seleccionar-departamento/seleccionar-departamento.component";
import {SeleccionarProductoComponent} from "../../componentes/seleccionar-producto/seleccionar-producto.component";
import {ModalAgregarProductoInventarioComponent} from "./componentes/modal-agregar-producto-inventario/modal-agregar-producto-inventario.component";
import {ModalNuevoMantenimientoComponent} from "./componentes/modal-nuevo-mantenimiento/modal-nuevo-mantenimiento.component";
import {SeleccionarArticuloInventarioComponent} from "./componentes/seleccionar-articulo-inventario/seleccionar-articulo-inventario.component";
import {FormularioNuevoMantenimientoComponent} from "./componentes/formulario-nuevo-mantenimiento/formulario-nuevo-mantenimiento.component";
import {RutaArticuloInventarioComponent} from "./rutas/ruta-articulo-inventario/ruta-articulo-inventario.component";
import {FormularioBodegaInventarioComponent} from "./componentes/formulario-bodega-inventario/formulario-bodega-inventario.component";
import {ModalCrearEditarBodegaInventarioComponent} from "./componentes/modal-crear-editar-bodega-inventario/modal-crear-editar-bodega-inventario.component";
import { ModalIngresoEgresoComponent } from './componentes/modal-ingreso-egreso/modal-ingreso-egreso.component';
import {FormularioProductoInventarioComponent} from './componentes/formulario-producto-inventario/formulario-producto.component';


@NgModule({
  declarations: [
    RutaInventarioBodegaComponent,
    RutaMantenimientoInventarioComponent,
    SeleccionarBodegaComponent,
    SeleccionarDepartamentoComponent,
    ModalAgregarProductoInventarioComponent,
    ModalNuevoMantenimientoComponent,
    SeleccionarArticuloInventarioComponent,
    FormularioNuevoMantenimientoComponent,
    RutaArticuloInventarioComponent,
    FormularioBodegaInventarioComponent,
    ModalCrearEditarBodegaInventarioComponent,
    ModalIngresoEgresoComponent,
    FormularioProductoInventarioComponent
  ],
  imports: [
    CommonModule,
    InventarioRoutingModule,
    ...MODULOS_COMUNES,

  ]
})
export class InventarioModule { }
