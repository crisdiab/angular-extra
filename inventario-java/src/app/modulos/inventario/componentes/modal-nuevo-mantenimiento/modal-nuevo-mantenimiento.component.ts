import {Component, Inject, OnInit} from '@angular/core';
import {DepartamentoInterface} from '../../../departamento/interfaces/departamento.interface';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {MantenimientoInterface} from '../../interfaces/mantenimiento.interface';
import {MantenimientoRestService} from '../../../../servicios/rest/mantenimiento-rest.service';

@Component({
  selector: 'app-modal-nuevo-mantenimiento',
  templateUrl: './modal-nuevo-mantenimiento.component.html',
  styleUrls: ['./modal-nuevo-mantenimiento.component.scss']
})
export class ModalNuevoMantenimientoComponent implements OnInit {
  mantenimientoACrear?: MantenimientoInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      mantenimiento: MantenimientoInterface | undefined,
      idArticuloInventario: number
    },
    private readonly _mantenimientoRestService: MantenimientoRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
  }

  guardarRegistro() {
    this._cargandoService.habilitarCargando();
    if (this.data.mantenimiento) {
      // @ts-ignore
      this.mantenimientoACrear.id = this.data.mantenimiento.id;
      this._mantenimientoRestService
        .editar(this.mantenimientoACrear as DepartamentoInterface, this.data.mantenimiento.id as number)
        .subscribe(
          creado => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Se actualizó correctamente');
            this._dialog.closeAll();
          },
          error => {
            console.error({
              error
            });
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        );
    } else {
      if(this.mantenimientoACrear) {
        this.mantenimientoACrear.articuloInventario = {id: this.data.idArticuloInventario}
        this._mantenimientoRestService
          .crear(this.mantenimientoACrear as DepartamentoInterface)
          .subscribe(
            creado => {
              this._cargandoService.deshabilitarCargando();
              this._toastrService.success('Se creó correctamente');
              this._dialog.closeAll();
            },
            error => {
              console.error({
                error
              });
              this._cargandoService.deshabilitarCargando();
              this._toastrService.error('Ocurrió un error', 'Error');
            }
          );
      }


    }
  }

  setearMantenimiento(mantenimiento: MantenimientoInterface | undefined) {
    this.mantenimientoACrear = mantenimiento;
  }
}
