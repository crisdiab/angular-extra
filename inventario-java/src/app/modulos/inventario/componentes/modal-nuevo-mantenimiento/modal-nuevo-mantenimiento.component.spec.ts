import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNuevoMantenimientoComponent } from './modal-nuevo-mantenimiento.component';

describe('ModalNuevoMantenimientoComponent', () => {
  let component: ModalNuevoMantenimientoComponent;
  let fixture: ComponentFixture<ModalNuevoMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalNuevoMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNuevoMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
