import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DepartamentoInterface} from "../../../departamento/interfaces/departamento.interface";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {DepartamentoRestService} from "../../../../servicios/rest/departamento-rest.service";
import {map, startWith} from "rxjs/operators";
import {BodegaInterface} from "../../../bodega/interfaces/bodega.interface";
import {BodegaRestService} from "../../../../servicios/rest/bodega-rest.service";
import {CargandoService} from '../../../../servicios/cargando.service';
import {PAGINACION} from '../../../../constantes/skip-take';

@Component({
  selector: 'app-seleccionar-bodega',
  templateUrl: './seleccionar-bodega.component.html',
  styleUrls: ['./seleccionar-bodega.component.scss']
})
export class SeleccionarBodegaComponent implements OnInit {


  @Output()
  seleccionoBodega: EventEmitter<BodegaInterface> = new EventEmitter<BodegaInterface>();

  arregloBodegas: BodegaInterface[] = [];
  bodegaSeleccionado?: BodegaInterface;

  controlAutocomplete = new FormControl();
  opcionesFiltradas: Observable<BodegaInterface[]> | undefined;

  constructor(
    private readonly _bodegaRestService: BodegaRestService,
    private readonly _cargandoService: CargandoService
  ) {
  }

  ngOnInit(): void {
    this._cargarBodegas();
    this.escucharCambiosAutocomplete();

  }

  private _cargarBodegas() {
    this._cargandoService.habilitarCargando();
    const busqueda = {
      skip: 0,
      take: 100,
      busqueda: ''
    };
    this._bodegaRestService
      .buscar(busqueda)
      .subscribe(
        (r: any) => {
          this._cargandoService.deshabilitarCargando();
          this.arregloBodegas = r.listaObjetos;
        },
        error => {
          this._cargandoService.deshabilitarCargando();
          console.error('Ocurrio un error', error)
        }
      );

  }

  emitirSeleccionoBodega(valor?: BodegaInterface) {
    this.seleccionoBodega.emit(valor);
  }

  escucharCambiosAutocomplete() {
    this.opcionesFiltradas = this.controlAutocomplete
      .valueChanges
      .pipe(
        startWith(''),
        map(value => this._filtrarArreglo(value))
      );
  }

  private _filtrarArreglo(value: string): BodegaInterface[] {
    const valorFiltrado = value;
    return this.arregloBodegas
      .filter(option => {
        const busqueda = option['nombre'] as string;
        return busqueda.search(valorFiltrado) >= 0;
      });
  }

  mostrarNombreBodega() {
    return (val: any) => this.display(val);
  }

  private display(bodega: BodegaInterface): any {
    return bodega ? bodega.nombre : bodega;
  }

}
