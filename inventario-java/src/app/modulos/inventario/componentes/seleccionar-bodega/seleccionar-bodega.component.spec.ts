import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarBodegaComponent } from './seleccionar-bodega.component';

describe('SeleccionarBodegaComponent', () => {
  let component: SeleccionarBodegaComponent;
  let fixture: ComponentFixture<SeleccionarBodegaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionarBodegaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarBodegaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
