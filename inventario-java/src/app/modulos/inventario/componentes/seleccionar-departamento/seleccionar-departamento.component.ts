import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DepartamentoInterface} from "../../../departamento/interfaces/departamento.interface";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {DepartamentoRestService} from "../../../../servicios/rest/departamento-rest.service";
import {map, startWith} from "rxjs/operators";

@Component({
  selector: 'app-seleccionar-departamento',
  templateUrl: './seleccionar-departamento.component.html',
  styleUrls: ['./seleccionar-departamento.component.scss']
})
export class SeleccionarDepartamentoComponent implements OnInit {


  @Output()
  seleccionoDepartamento: EventEmitter<DepartamentoInterface> = new EventEmitter<DepartamentoInterface>();

  arregloDepartamentos: DepartamentoInterface[] = [];
  departamentoSeleccionado?: DepartamentoInterface;

  controlAutocomplete = new FormControl();
  opcionesFiltradas: Observable<DepartamentoInterface[]> | undefined;

  constructor(
    private readonly _departamentoRestService: DepartamentoRestService
  ) {
  }

  ngOnInit(): void {
    this._cargarDepartamentos();
    this.escucharCambiosAutocomplete();

  }

  private _cargarDepartamentos() {

  }

  emitirSeleccionoDepartamento(valor?: DepartamentoInterface) {
    this.seleccionoDepartamento.emit(valor);
  }

  escucharCambiosAutocomplete() {
    this.opcionesFiltradas = this.controlAutocomplete
      .valueChanges
      .pipe(
        startWith(''),
        map(value => this._filtrarArreglo(value))
      );
  }

  private _filtrarArreglo(value: string): DepartamentoInterface[] {
    const valorFiltrado = value;
    return this.arregloDepartamentos
      .filter(option =>
      {
        const busqueda = option['nombre'] as string;
        return busqueda.search(valorFiltrado) >= 0
      });
  }
  mostrarNombreDepartamento() {
    return (val: any) => this.display(val);
  }

  private display(departamento: DepartamentoInterface): any {
    return departamento ? departamento.nombre : departamento;
  }

}
