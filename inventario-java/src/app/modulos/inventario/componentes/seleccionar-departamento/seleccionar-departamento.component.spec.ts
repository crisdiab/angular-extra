import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarDepartamentoComponent } from './seleccionar-departamento.component';

describe('SeleccionarDepartamentoComponent', () => {
  let component: SeleccionarDepartamentoComponent;
  let fixture: ComponentFixture<SeleccionarDepartamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionarDepartamentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarDepartamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
