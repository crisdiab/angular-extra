import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  MENSAJES_VALIDACION_CODIGO,
  MENSAJES_VALIDACION_CODIGO_AUXILIAR,
  MENSAJES_VALIDACION_DESCRIPCION,
  MENSAJES_VALIDACION_NOMBRE
} from '../../../bodega/constantes/mensajes-validacion';
import {Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {ProductoInterface} from '../../../producto/interfaces/producto.interface';
import {ArticuloInventarioInterface} from '../../interfaces/articulo-inventario.interface';

@Component({
  selector: 'app-formulario-producto-inventario',
  templateUrl: './formulario-producto.component.html',
  styleUrls: ['./formulario-producto.component.scss']
})
export class FormularioProductoInventarioComponent implements OnInit {

  formularioProducto?: FormGroup;
  formularioValido = false;
  @Input() producto?: ArticuloInventarioInterface;
  mensajesErrorNombre: string[] = [];
  mensajesErrorCodigo: string[] = [];
  mensajesErrorCodigoAuxiliar: string[] = [];
  mensajesErrorDescripcion: string[] = [];
  private _mensajesValidacionNombre = MENSAJES_VALIDACION_NOMBRE;
  private _mensajesValidacionCodigo = MENSAJES_VALIDACION_CODIGO
  private _mensajesValidacionCodigoAuxiliar = MENSAJES_VALIDACION_CODIGO_AUXILIAR
  private _mensajesValidacionDescripcion = MENSAJES_VALIDACION_DESCRIPCION

  subscriptores: Subscription[] = [];

  @Output() formularioEmitter: EventEmitter<ProductoInterface | undefined> = new EventEmitter<ProductoInterface | undefined>();

  constructor(private readonly _formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.iniciarFormulario();
    this._escucharFormulario();
    this._escucharCampoNombre();
    this._escucharCampoCodigo();
    this._escucharCampoCodigoAuxiliar();
    this._escucharCampoDescripcion();

  }

  ngOnDestroy(): void {
    this.subscriptores.forEach(valor => valor.unsubscribe());
  }

  iniciarFormulario(){
    this.formularioProducto = this._formBuilder.group(
      {
        nombre: [this.producto ? this.producto.nombre : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ],
        ],
        codigo: [this.producto ? this.producto.codigo : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(10),
          ],
        ],
        codigoAuxiliar: [this.producto ? this.producto.codigoAuxiliar : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(10),
          ],
        ],
        descripcion: [this.producto ? this.producto.descripcion : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(250),
          ],
        ],

      })
  }

  private _escucharFormulario() {
    // el observable que escucha cambios
  if(this.formularioProducto){
    const observableCambiosFormulario$ = this.formularioProducto.valueChanges
    const observableFOmrulario = observableCambiosFormulario$
      .subscribe((formulario) => {
        // console.log('escuchando cambios formulario', formulario)
        if (this.formularioProducto?.valid) {
          this.formularioEmitter.emit(formulario)
        } else {
          this.formularioEmitter.emit(undefined)
        }
      })
    this.subscriptores.push(observableFOmrulario);
  }
  }

  private _escucharCampoNombre() {
    const controlNombre = this.formularioProducto?.get('nombre') as AbstractControl;

    const observableDeCambiosNombre$ = controlNombre.valueChanges;

    const respuestaObservableNombre = observableDeCambiosNombre$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesNombre(controlNombre)
      })
    this.subscriptores.push(respuestaObservableNombre);
  }

  private _setearMensajesNombre(controlForm: AbstractControl) {
    this.mensajesErrorNombre = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorNombre = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' | 'pattern' = atributo as 'required' | 'minlength' | 'maxlength' | 'pattern'
          return this._mensajesValidacionNombre[mensajeError]
        })
    }
  }

  private _escucharCampoCodigo() {
    const controlCodigo = this.formularioProducto?.get('codigo') as AbstractControl;

    const observableDeCambiosCodigo$ = controlCodigo.valueChanges;

    const respuestaObservableCodigo = observableDeCambiosCodigo$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesCodigo(controlCodigo)
      })
    this.subscriptores.push(respuestaObservableCodigo);
  }

  private _setearMensajesCodigo(controlForm: AbstractControl) {
    this.mensajesErrorCodigo = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorCodigo = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength'
          return this._mensajesValidacionCodigo[mensajeError]
        })
    }
  }

  private _escucharCampoCodigoAuxiliar() {
    const controlCodigoAuxiliar = this.formularioProducto?.get('codigoAuxiliar') as AbstractControl;

    const observableDeCambiosCodigoAuxiliar$ = controlCodigoAuxiliar.valueChanges;

    const respuestaObservableCodigoAuxiliar = observableDeCambiosCodigoAuxiliar$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesCodigoAuxiliar(controlCodigoAuxiliar)
      })
    this.subscriptores.push(respuestaObservableCodigoAuxiliar);
  }

  private _setearMensajesCodigoAuxiliar(controlForm: AbstractControl) {
    this.mensajesErrorCodigoAuxiliar = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorCodigo = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as  'required' |'minlength' | 'maxlength'
          return this._mensajesValidacionCodigoAuxiliar[mensajeError]
        })
    }
  }

  private _escucharCampoDescripcion() {
    const controlDescripcion = this.formularioProducto?.get('descripcion') as AbstractControl;

    const observableDeCambiosDescriocion$ = controlDescripcion.valueChanges;

    const respuestaObservableDescripcion = observableDeCambiosDescriocion$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesDescripcion(controlDescripcion)
      })
    this.subscriptores.push(respuestaObservableDescripcion);
  }

  private _setearMensajesDescripcion(controlForm: AbstractControl) {
    this.mensajesErrorDescripcion = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorDescripcion = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength'
          return this._mensajesValidacionDescripcion[mensajeError]
        })
    }
  }

}
