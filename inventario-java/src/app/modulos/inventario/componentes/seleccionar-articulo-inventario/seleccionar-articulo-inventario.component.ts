import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BodegaInterface} from "../../../bodega/interfaces/bodega.interface";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {map, startWith} from "rxjs/operators";
import {ArticuloInventarioInterface} from "../../interfaces/articulo-inventario.interface";
import {ArticuloInventarioRestService} from "../../../../servicios/rest/articulo-inventario-rest.service";
import {ProductoInterface} from "../../../producto/interfaces/producto.interface";

@Component({
  selector: 'app-seleccionar-articulo-inventario',
  templateUrl: './seleccionar-articulo-inventario.component.html',
  styleUrls: ['./seleccionar-articulo-inventario.component.scss']
})
export class SeleccionarArticuloInventarioComponent implements OnInit {
  @Output()
  seleccionoArticuloInventario: EventEmitter<ArticuloInventarioInterface> = new EventEmitter<ArticuloInventarioInterface>();

  arregloArticulosInventario: ArticuloInventarioInterface[] = [];
  articuloInventarioSeleccionado?: ArticuloInventarioInterface;

  controlAutocomplete = new FormControl();
  opcionesFiltradas: Observable<ArticuloInventarioInterface[]> | undefined;

  constructor(
    private readonly _articuloInventarioRestService: ArticuloInventarioRestService
  ) {
  }

  ngOnInit(): void {
    this._cargarArticulosInventario();
    this.escucharCambiosAutocomplete();

  }

  private _cargarArticulosInventario() {

  }

  emitirSeleccionoArticuloInventario(valor?: BodegaInterface) {
    this.seleccionoArticuloInventario.emit(valor);
  }

  escucharCambiosAutocomplete() {
    this.opcionesFiltradas = this.controlAutocomplete
      .valueChanges
      .pipe(
        startWith(''),
        map(value => this._filtrarArreglo(value))
      );
  }

  private _filtrarArreglo(value: string): ArticuloInventarioInterface[] {
    const valorFiltrado = value;
    return this.arregloArticulosInventario
      .filter(option => {
        const productoBusqueda = option.producto as ProductoInterface
        const busqueda = productoBusqueda.nombre as string;
        return busqueda.search(valorFiltrado) >= 0
      });
  }

  mostrarNombreBodega() {
    return (val: any) => this.display(val);
  }

  private display(articuloInventario: ArticuloInventarioInterface): any {
    const productoBusqueda = articuloInventario.producto as ProductoInterface
    const nombreProducto = productoBusqueda.nombre
    return articuloInventario ? articuloInventario.nombreProducto : articuloInventario;
  }


}
