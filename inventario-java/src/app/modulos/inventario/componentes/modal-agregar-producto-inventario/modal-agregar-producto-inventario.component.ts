import {Component, Inject, OnInit} from '@angular/core';
import {ProductoInterface} from "../../../producto/interfaces/producto.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {BodegaInventarioRestService} from "../../../../servicios/rest/bodega-inventario-rest.service";
import {BodegaInventarioInterface} from "../../interfaces/bodega-inventario.interface";
import {ArticuloInventarioInterface} from '../../interfaces/articulo-inventario.interface';
import {ToastrService} from 'ngx-toastr';
import {ArticuloInventarioRestService} from '../../../../servicios/rest/articulo-inventario-rest.service';
import {BodegaInterface} from '../../../bodega/interfaces/bodega.interface';

@Component({
  selector: 'app-modal-agregar-producto-inventario',
  templateUrl: './modal-agregar-producto-inventario.component.html',
  styleUrls: ['./modal-agregar-producto-inventario.component.scss']
})
export class ModalAgregarProductoInventarioComponent implements OnInit {

  productoSeleccionados?: ProductoInterface;
  articuloInventario?: number;
  articuloInventarioDatos?: ArticuloInventarioInterface;
  puedeAgregarStock = true;

  constructor(
    private readonly _dialogRef: MatDialogRef<ModalAgregarProductoInventarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      idBodegaInventario: BodegaInventarioInterface,
      articuloInventario: ArticuloInventarioInterface,
      tipoBodega: { nombre?: string, descripcion?: string, esMantenimiento?: boolean }
    },
    private readonly _inventarioProductoRestService: ArticuloInventarioRestService,
    private readonly _toastrService: ToastrService
  ) {
    if (data.tipoBodega) {
      if(data.tipoBodega.esMantenimiento ) {
        this.articuloInventario = 1;
        this.puedeAgregarStock = false;
      }

    }
  }

  ngOnInit(): void {
  }

  setearProductoSeleccionado(producto: ProductoInterface) {
    this.productoSeleccionados = producto;
  }

  guardarRegistro() {

    if (this.articuloInventarioDatos) {
      if (this.data.articuloInventario) {
        this.articuloInventarioDatos.id = this.data.articuloInventario.id;
        this._inventarioProductoRestService
          .editar(this.articuloInventarioDatos, this.data.articuloInventario.id as number)
          .subscribe(
            (r) => {
              this._dialogRef.close();
              this._toastrService.success('Modificado correctamente', 'Correcto');
            },
            error => {
              this._toastrService.error('Ocurrió un error', 'Error');
              console.error(error);
            }
          );
      } else {

        this.articuloInventarioDatos.bodegaInventario = {id: this.data.idBodegaInventario as number};
        this.articuloInventarioDatos.producto = {id: this.productoSeleccionados?.id};
        this.articuloInventarioDatos.stock = this.articuloInventario;
        this.articuloInventarioDatos.habilitado = true;
        this._inventarioProductoRestService.crear(this.articuloInventarioDatos)
          .subscribe(
            (r) => {
              this._dialogRef.close();
              this._toastrService.success('Creado correctamente', 'Correcto');
            },
            error => {
              this._toastrService.error('Ocurrió un error', 'Error');
              console.error(error);
            }
          );
      }

    }


  }

  setearDatosArticulo(evento: any) {
    this.articuloInventarioDatos = evento;
  }
}


