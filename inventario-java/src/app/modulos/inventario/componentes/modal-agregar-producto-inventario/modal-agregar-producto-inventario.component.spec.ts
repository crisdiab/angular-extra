import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAgregarProductoInventarioComponent } from './modal-agregar-producto-inventario.component';

describe('ModalAgregarProductoInventarioComponent', () => {
  let component: ModalAgregarProductoInventarioComponent;
  let fixture: ComponentFixture<ModalAgregarProductoInventarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAgregarProductoInventarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAgregarProductoInventarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
