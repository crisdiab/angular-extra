import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductoInterface} from "../../../producto/interfaces/producto.interface";
import {
  MENSAJES_VALIDACION_CODIGO,
  MENSAJES_VALIDACION_CODIGO_AUXILIAR, MENSAJES_VALIDACION_DESCRIPCION, MENSAJES_VALIDACION_IMAGENURL,
  MENSAJES_VALIDACION_NOMBRE
} from "../../../bodega/constantes/mensajes-validacion";
import {Subscription} from "rxjs";
import {debounceTime} from "rxjs/operators";
import {BodegaInventarioInterface} from "../../interfaces/bodega-inventario.interface";

@Component({
  selector: 'app-formulario-bodega-inventario',
  templateUrl: './formulario-bodega-inventario.component.html',
  styleUrls: ['./formulario-bodega-inventario.component.scss']
})
export class FormularioBodegaInventarioComponent implements OnInit {

  formularioBodegaInventario?: FormGroup;
  formularioValido = false;
  @Input() bodegaInventario?: BodegaInventarioInterface;
  mensajesErrorNombre: string[] = [];
  mensajesErrorCodigo: string[] = [];
  mensajesErrorDireccion: string[] = [];
  mensajesErrorDescripcion: string[] = [];
  private _mensajesValidacionNombre = MENSAJES_VALIDACION_NOMBRE;
  private _mensajesValidacionCodigo = MENSAJES_VALIDACION_CODIGO;
  private _mensajesValidacionDireccion = MENSAJES_VALIDACION_CODIGO_AUXILIAR;
  private _mensajesValidacionDescripcion = MENSAJES_VALIDACION_DESCRIPCION;

  subscriptores: Subscription[] = [];

  @Output() formularioEmitter: EventEmitter<BodegaInventarioInterface | undefined> = new EventEmitter<BodegaInventarioInterface | undefined>();

  constructor(private readonly _formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.iniciarFormulario();
    this._escucharFormulario();
    this._escucharCampoNombre();
    this._escucharCampoCodigo();
    this._escucharCampoDireccion();
    this._escucharCampoDescripcion();

  }

  ngOnDestroy(): void {
    this.subscriptores.forEach(valor => valor.unsubscribe());
  }

  iniciarFormulario() {
    this.formularioBodegaInventario = this._formBuilder.group(
      {
        nombre: [this.bodegaInventario ? this.bodegaInventario.nombre : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ],
        ],
        codigo: [this.bodegaInventario ? this.bodegaInventario.codigo : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(10),
          ],
        ],
        direccion: [this.bodegaInventario ? this.bodegaInventario.direccion : '',
          [
            Validators.minLength(3),
            Validators.maxLength(250),
          ],
        ],
        descripcion: [this.bodegaInventario ? this.bodegaInventario.descripcion : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(200),
          ],
        ],
      });
  }

  private _escucharFormulario() {
    if (this.formularioBodegaInventario) {
      // el observable que escucha cambios
      const observableCambiosFormulario$ = this.formularioBodegaInventario.valueChanges;
      const observableFOmrulario = observableCambiosFormulario$
        .subscribe((formulario: any) => {
          // console.log('escuchando cambios formulario', formulario)
          if (this.formularioBodegaInventario?.valid) {
            this.formularioEmitter.emit(formulario);
          } else {
            this.formularioEmitter.emit(undefined);
          }
        });
      this.subscriptores.push(observableFOmrulario);
    }

  }

  private _escucharCampoNombre() {
    const controlNombre = this.formularioBodegaInventario?.get('nombre') as AbstractControl;

    const observableDeCambiosNombre$ = controlNombre.valueChanges;

    const respuestaObservableNombre = observableDeCambiosNombre$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesNombre(controlNombre);
      });
    this.subscriptores.push(respuestaObservableNombre);
  }

  private _setearMensajesNombre(controlForm: AbstractControl) {
    this.mensajesErrorNombre = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorNombre = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' | 'pattern' = atributo as 'required' | 'minlength' | 'maxlength' | 'pattern';
          return this._mensajesValidacionNombre[mensajeError];
        });
    }
  }

  private _escucharCampoCodigo() {
    const controlCodigo = this.formularioBodegaInventario?.get('codigo') as AbstractControl;

    const observableDeCambiosCodigo$ = controlCodigo.valueChanges;

    const respuestaObservableCodigo = observableDeCambiosCodigo$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesCodigo(controlCodigo);
      });
    this.subscriptores.push(respuestaObservableCodigo);
  }

  private _setearMensajesCodigo(controlForm: AbstractControl) {
    this.mensajesErrorCodigo = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorCodigo = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength';
          return this._mensajesValidacionCodigo[mensajeError];
        });
    }
  }

  private _escucharCampoDireccion() {
    const controlDireccion = this.formularioBodegaInventario?.get('direccion') as AbstractControl;

    const observableDeCambiosDireccion$ = controlDireccion.valueChanges;

    const respuestaObservableDireccion = observableDeCambiosDireccion$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesDireccion(controlDireccion);
      });
    this.subscriptores.push(respuestaObservableDireccion);
  }

  private _setearMensajesDireccion(controlForm: AbstractControl) {
    this.mensajesErrorDireccion = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorCodigo = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'minlength' | 'maxlength' = atributo as 'minlength' | 'maxlength';
          return this._mensajesValidacionDireccion[mensajeError];
        });
    }
  }

  private _escucharCampoDescripcion() {
    const controlDescripcion = this.formularioBodegaInventario?.get('descripcion') as AbstractControl;

    const observableDeCambiosDescriocion$ = controlDescripcion.valueChanges;

    const respuestaObservableDescripcion = observableDeCambiosDescriocion$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesDescripcion(controlDescripcion);
      });
    this.subscriptores.push(respuestaObservableDescripcion);
  }

  private _setearMensajesDescripcion(controlForm: AbstractControl) {
    this.mensajesErrorDescripcion = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorDescripcion = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength';
          return this._mensajesValidacionDescripcion[mensajeError];
        });
    }
  }

}
