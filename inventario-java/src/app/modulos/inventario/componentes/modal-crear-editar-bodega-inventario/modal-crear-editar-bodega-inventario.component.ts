import {Component, Inject, OnInit} from '@angular/core';
import {BodegaInterface} from "../../../bodega/interfaces/bodega.interface";
import {DepartamentoInterface} from "../../../departamento/interfaces/departamento.interface";
import {BodegaInventarioInterface} from '../../interfaces/bodega-inventario.interface';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {BodegaInventarioRestService} from '../../../../servicios/rest/bodega-inventario-rest.service';

@Component({
  selector: 'app-modal-crear-editar-bodega-inventario',
  templateUrl: './modal-crear-editar-bodega-inventario.component.html',
  styleUrls: ['./modal-crear-editar-bodega-inventario.component.scss']
})
export class ModalCrearEditarBodegaInventarioComponent implements OnInit {

  bodegaSeleccionado: BodegaInterface | undefined;
  departamentoSeleccionado: DepartamentoInterface | undefined;
  inventarioCrearEditar?: BodegaInventarioInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      bodegaInventario: BodegaInventarioInterface | undefined,
      idDepartamento: number
    },
    private readonly _bodegaInventario: BodegaInventarioRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
  }

  guardarRegistro() {
    this._cargandoService.habilitarCargando();
    if (this.data.bodegaInventario) {
      // @ts-ignore
      this.inventarioCrearEditar?.habilitado = this.data.bodegaInventario.habilitado;
      // @ts-ignore
      this.inventarioCrearEditar?.id = this.data.bodegaInventario.id;
      this._bodegaInventario
        .editar(this.inventarioCrearEditar as BodegaInventarioInterface, this.data.bodegaInventario.id as number)
        .subscribe(
          creado => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Se actualizó correctamente');
            this._dialog.closeAll();
          },
          error => {
            console.error({
              error
            });
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        );
    } else {
      if(this.inventarioCrearEditar && this.bodegaSeleccionado){
        this.inventarioCrearEditar.departamento = {id:this.data.idDepartamento};
        this.inventarioCrearEditar.tipoBodega = {id:this.bodegaSeleccionado.id};
        this.inventarioCrearEditar.habilitado = true;
        this.inventarioCrearEditar.esMantenimiento = this.bodegaSeleccionado.esMantenimiento;
        this._bodegaInventario
          .crear(this.inventarioCrearEditar as BodegaInterface)
          .subscribe(
            creado => {
              this._cargandoService.deshabilitarCargando();
              this._toastrService.success('Se creó correctamente');
              this._dialog.closeAll();
            },
            error => {
              console.error({
                error
              });
              this._cargandoService.deshabilitarCargando();
              this._toastrService.error('Ocurrió un error', 'Error');
            }
          );
      }

    }


  }

  setearBodegaInventario(bodegaInventario: BodegaInventarioInterface | undefined) {
    this.inventarioCrearEditar = bodegaInventario;
  }

  setearBodega(bodegaSeleccionada: BodegaInterface) {
    this.bodegaSeleccionado = bodegaSeleccionada;
  }

}
