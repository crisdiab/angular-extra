import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarBodegaInventarioComponent } from './modal-crear-editar-bodega-inventario.component';

describe('ModalCrearEditarBodegaInventarioComponent', () => {
  let component: ModalCrearEditarBodegaInventarioComponent;
  let fixture: ComponentFixture<ModalCrearEditarBodegaInventarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarBodegaInventarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarBodegaInventarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
