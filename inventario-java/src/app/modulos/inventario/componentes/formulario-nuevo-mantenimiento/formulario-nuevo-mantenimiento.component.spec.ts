import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioNuevoMantenimientoComponent } from './formulario-nuevo-mantenimiento.component';

describe('FormularioNuevoMantenimientoComponent', () => {
  let component: FormularioNuevoMantenimientoComponent;
  let fixture: ComponentFixture<FormularioNuevoMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioNuevoMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioNuevoMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
