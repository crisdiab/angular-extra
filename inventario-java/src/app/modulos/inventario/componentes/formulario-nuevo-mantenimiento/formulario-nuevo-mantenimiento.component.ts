import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BodegaInterface} from "../../../bodega/interfaces/bodega.interface";
import {
  MENSAJES_VALIDACION_CEDULA,
  MENSAJES_VALIDACION_CODIGO, MENSAJES_VALIDACION_DESCRIPCION,
  MENSAJES_VALIDACION_DIRECCION,
  MENSAJES_VALIDACION_NOMBRE, MENSAJES_VALIDACION_TIPO_MANTENIMIENTO
} from "../../../bodega/constantes/mensajes-validacion";
import {Subscription} from "rxjs";
import {debounceTime} from "rxjs/operators";
import {MantenimientoInterface} from "../../interfaces/mantenimiento.interface";
import {BodegaInventarioInterface} from "../../interfaces/bodega-inventario.interface";
import {validarCedula} from '../../../usuario/componentes/validar-cedula';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-formulario-nuevo-mantenimiento',
  templateUrl: './formulario-nuevo-mantenimiento.component.html',
  styleUrls: ['./formulario-nuevo-mantenimiento.component.scss']
})
export class FormularioNuevoMantenimientoComponent implements OnInit {

  articuloBodegaSeleccionado: BodegaInventarioInterface | undefined;
  formularioMantenimiento?: FormGroup;
  formularioValido = false;
  @Input() mantenimiento?: MantenimientoInterface;
  mensajesErrorNombre: string[] = [];
  mensajesErrorTipoMantenimiento: string[] = [];
  mensajesErrorDescripcion: string[] = [];
  mensajesErrorCedula: string[] = [];
  private _mensajesValidacionNombre = MENSAJES_VALIDACION_NOMBRE;
  private _mensajesValidacionCedula = MENSAJES_VALIDACION_CEDULA;
  private _mensajesValidacionTipoMantenimiento = MENSAJES_VALIDACION_TIPO_MANTENIMIENTO
  private _mensajesValidacionDescripcion = MENSAJES_VALIDACION_DESCRIPCION

  subscriptores: Subscription[] = [];

  @Output() formularioEmitter: EventEmitter<MantenimientoInterface | undefined> = new EventEmitter<MantenimientoInterface | undefined>();

  constructor(private readonly _formBuilder: FormBuilder,
              private readonly _toasterService: ToastrService) {

  }

  ngOnInit(): void {
    this.iniciarFormulario();
    this._escucharFormulario();
    this._escucharCampoNombre();
    this._escucharCampoTipoMantenimiento()
    this._escucharCampoDescripcion();
    this._escucharCampoCedula();

  }

  ngOnDestroy(): void {
    this.subscriptores.forEach(valor => valor.unsubscribe());
  }

iniciarFormulario () {
  this.formularioMantenimiento = this._formBuilder.group(
    {
      nombreTecnico: [this.mantenimiento ? this.mantenimiento.nombreTecnico : '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ],
      ],
      tipoMantenimiento: [this.mantenimiento ? this.mantenimiento.tipoMantenimiento : '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10),
        ],
      ],
      descripcion: [this.mantenimiento ? this.mantenimiento.descripcion : '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(250),
        ],
      ],
      cedulaTecnico: [this.mantenimiento ? this.mantenimiento.cedulaTecnico : '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
    })
}
  private _escucharFormulario() {
    // el observable que escucha cambios
    if(this.formularioMantenimiento) {
      const observableCambiosFormulario$ = this.formularioMantenimiento.valueChanges
      const observableFOmrulario = observableCambiosFormulario$
        .subscribe((formulario) => {
          if (this.formularioMantenimiento?.valid) {
            if( validarCedula(formulario.cedulaTecnico)){
              this.formularioEmitter.emit(formulario);
            }else {
              this.formularioEmitter.emit(undefined);
              const controCedula = this.formularioMantenimiento?.get('cedulaTecnico') as AbstractControl;
              controCedula.reset();
              this._toasterService.error('La cédula no es válida', 'Error')
            }

          } else {
            this.formularioEmitter.emit(undefined)
          }
        })
      this.subscriptores.push(observableFOmrulario);
    }

  }

  private _escucharCampoNombre() {
    const controlNombre = this.formularioMantenimiento?.get('nombreTecnico') as AbstractControl;

    const observableDeCambiosNombre$ = controlNombre.valueChanges;

    const respuestaObservableNombre = observableDeCambiosNombre$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesNombre(controlNombre)
      })
    this.subscriptores.push(respuestaObservableNombre);
  }

  private _setearMensajesNombre(controlForm: AbstractControl) {
    this.mensajesErrorNombre = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorNombre = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' | 'pattern' = atributo as 'required' | 'minlength' | 'maxlength' | 'pattern'
          return this._mensajesValidacionNombre[mensajeError]
        })
    }
  }

  private _escucharCampoTipoMantenimiento() {
    const controlTipoMantenimiento = this.formularioMantenimiento?.get('tipoMantenimiento') as AbstractControl;

    const observableDeCambiosTipoMantenimiento$ = controlTipoMantenimiento.valueChanges;

    const respuestaObservableTipoMantenimiento = observableDeCambiosTipoMantenimiento$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesTipoMantenimiento(controlTipoMantenimiento)
      })
    this.subscriptores.push(respuestaObservableTipoMantenimiento);
  }

  private _setearMensajesTipoMantenimiento(controlForm: AbstractControl) {
    this.mensajesErrorTipoMantenimiento = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorTipoMantenimiento = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' = atributo as 'required'
          return this._mensajesValidacionTipoMantenimiento[mensajeError]
        })
    }
  }


  private _escucharCampoDescripcion() {
    const controlDescripcion = this.formularioMantenimiento?.get('descripcion') as AbstractControl;

    const observableDeCambiosDescriocion$ = controlDescripcion.valueChanges;

    const respuestaObservableDescripcion = observableDeCambiosDescriocion$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesDescripcion(controlDescripcion)
      })
    this.subscriptores.push(respuestaObservableDescripcion);
  }

  private _setearMensajesDescripcion(controlForm: AbstractControl) {
    this.mensajesErrorDescripcion = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorDescripcion = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength'
          return this._mensajesValidacionDescripcion[mensajeError]
        })
    }
  }

  private _escucharCampoCedula() {
    const controCedula = this.formularioMantenimiento?.get('cedulaTecnico') as AbstractControl;

    const observableDeCambiosCedula$ = controCedula.valueChanges;

    const respuestaObservableCedula = observableDeCambiosCedula$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
console.log(respuesta)

        if( validarCedula(respuesta)){
          this._setearMensajesCedula(controCedula);
          this._toasterService.success('Cédula válida')
        }else {
          controCedula.reset()
          this._toasterService.error('La cédula no es válida', 'Error')
        }

      });
    this.subscriptores.push(respuestaObservableCedula);
  }

  private _setearMensajesCedula(controlForm: AbstractControl) {
    this.mensajesErrorCedula = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorCedula = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength';
          return this._mensajesValidacionCedula[mensajeError];
        });
    }
  }
}
