import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ArticuloInventarioInterface} from '../../interfaces/articulo-inventario.interface';
import {ArticuloInventarioRestService} from '../../../../servicios/rest/articulo-inventario-rest.service';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {KardexInterface} from '../../interfaces/kardex.interface';
import {ProductoInterface} from '../../../producto/interfaces/producto.interface';
import * as moment from 'moment';
@Component({
  selector: 'app-modal-ingreso-egreso',
  templateUrl: './modal-ingreso-egreso.component.html',
  styleUrls: ['./modal-ingreso-egreso.component.scss']
})
export class ModalIngresoEgresoComponent implements OnInit {
  ingresoEgresoSeleccionado = true;
  cantidadSeleccionada?: number;
  nombreArticulo?: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      articuloInventario: ArticuloInventarioInterface | undefined,
    },
    private readonly _articuloInventarioRestService: ArticuloInventarioRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _dialogRef: MatDialogRef<any>,
  ) {
    moment.locale('es')
  }

  ngOnInit(): void {
    if (this.data.articuloInventario) {
      const producto = this.data.articuloInventario.producto as ProductoInterface;
      this.nombreArticulo = producto.nombre;
    }

  }

  guardarRegistro() {
    if (this.data.articuloInventario) {
      const ingresoEgresoCrear: KardexInterface = {
        articuloInventario: { id: this.data.articuloInventario.id} ,
        cantidad: this.cantidadSeleccionada,
        suma: this.ingresoEgresoSeleccionado,
        // fechaCreacion: moment().format('DD-MM-YYYY')

      };
      this._cargandoService.habilitarCargando();
      this._articuloInventarioRestService
        .ingresoEgreso(ingresoEgresoCrear)
        .subscribe(
          r => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success(
              this.ingresoEgresoSeleccionado ? 'Ingresado correctamente' : 'Egresado correctamente',
              'Correcto'
            );
            this._dialogRef.close({
              cantidad: this.cantidadSeleccionada,
              suma : this.ingresoEgresoSeleccionado
            });
          },
          error => {
            this._toastrService.error(
              'No se puede actualizar, verifique la cantidad',
              'Error'
            );
          }
        );
    }

  }
}
