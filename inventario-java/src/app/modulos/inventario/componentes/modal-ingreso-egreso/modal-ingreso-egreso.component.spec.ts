import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalIngresoEgresoComponent } from './modal-ingreso-egreso.component';

describe('ModalIngresoEgresoComponent', () => {
  let component: ModalIngresoEgresoComponent;
  let fixture: ComponentFixture<ModalIngresoEgresoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalIngresoEgresoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalIngresoEgresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
