import {RouterModule, Routes} from "@angular/router";
import {RutaGestionProductosComponent} from "../producto/rutas/ruta-gestion-productos/ruta-gestion-productos.component";
import {NgModule} from "@angular/core";
import {RutaMantenimientoInventarioComponent} from "./rutas/ruta-mantenimiento-inventario/ruta-mantenimiento-inventario.component";
import {RutaInventarioBodegaComponent} from "./rutas/ruta-inventario-bodega/ruta-inventario-bodega.component";
import {RutaArticuloInventarioComponent} from './rutas/ruta-articulo-inventario/ruta-articulo-inventario.component';
import {DaMantenimientoGuard} from '../../servicios/guards/da-mantenimiento.guard';

const routes: Routes = [

  {
    path: ':idDepartamento/bodega-inventario',
    component: RutaInventarioBodegaComponent
  },
  {
    path: ':idBodegaInventario/articulos-inventario',
    component: RutaArticuloInventarioComponent
  },
  {
    path:':idArticuloInventario/mantenimiento',
    component: RutaMantenimientoInventarioComponent,
    canActivate: [DaMantenimientoGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventarioRoutingModule { }
