import {Component, OnInit} from '@angular/core';
import {TABLAS} from '../../../../constantes/tablas';
import {MatDialog} from '@angular/material/dialog';
import {ModalCrearEditarDepartamentoComponent} from '../../../departamento/componentes/modal-crear-editar-departamento/modal-crear-editar-departamento.component';
import {BodegaInventarioInterface} from '../../interfaces/bodega-inventario.interface';
import {ModalAgregarProductoInventarioComponent} from '../../componentes/modal-agregar-producto-inventario/modal-agregar-producto-inventario.component';
import {ModalCrearEditarBodegaInventarioComponent} from "../../componentes/modal-crear-editar-bodega-inventario/modal-crear-editar-bodega-inventario.component";
import {BodegaInventarioRestService} from '../../../../servicios/rest/bodega-inventario-rest.service';
import {ToastrService} from 'ngx-toastr';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BusquedaDepartamentos} from '../../../departamento/interfaces/busqueda-departamentos';
import {PAGINACION} from '../../../../constantes/skip-take';
import {encontrarIndice} from '../../../../funciones/encontrar-indice';

@Component({
  selector: 'app-ruta-inventario-bodega',
  templateUrl: './ruta-inventario-bodega.component.html',
  styleUrls: ['./ruta-inventario-bodega.component.scss']
})
export class RutaInventarioBodegaComponent implements OnInit {


  columnasPrimeTable = [
    {field: 'nombre', header: 'Nombre', width: '70'},
    {field: 'id', header: 'Acciones', width: '30'}];
  filas = TABLAS.rows;
  inventarios: BodegaInventarioInterface[] = [
    {
      id:1,
      codigo: '2134s',
      direccion: '10 de agosto',
      nombre: 'Inventario',
      habilitado: true,
      descripcion: 'Bodega solo de productos de oficina'
    }
  ];
  totalBodegaInventario = 0;
  idDepartamento?: number;
  textoBusqueda?: string;

  constructor(
    private readonly _dialog: MatDialog,
    private readonly _bodegaInventarioRestService: BodegaInventarioRestService,
    private readonly _toastrService: ToastrService,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router
  ) {
  }

  ngOnInit(): void {
    this.recuperarParametros();
  }

  recuperarParametros() {
    this._activatedRoute
      .params
      .subscribe(
        (parametros) => {
          this.idDepartamento = Number(parametros.idDepartamento)
          this.cargarDatos();
        }
      );
  }

  limpiarFiltroGeneral() {

  }


  cargarDatos(skip?: number, take?: number) {
    this._cargandoService.habilitarCargando();
    let busqueda: BusquedaDepartamentos = {
      skip: skip ? skip : 0,
      take: take ? take : PAGINACION.take,
      busqueda: '',
      idDepartamento: this.idDepartamento as number
    };
    if(this.textoBusqueda !== '' && this.textoBusqueda) {
      busqueda.busqueda = this.textoBusqueda
    }
    console.log('busqueda', busqueda)
    this._bodegaInventarioRestService.buscar(busqueda)
      .subscribe(
        (datos: any) => {
          this._cargandoService.deshabilitarCargando();
          this.inventarios = datos.listaObjetos;
          this.totalBodegaInventario = this.textoBusqueda !== '' ? datos.cantidadRegistros : this.inventarios.length;
        },
        error => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.error('Ocurrió un error', 'Error');
        }
      );

  }

  cargarMasDatos(evento: any) {
    this.cargarDatos(evento.first, evento.rows);
  }

  abrirModalCrearEditar(bodegaInventario?: BodegaInventarioInterface | undefined) {
    const modalCargarArchivo = this._dialog.open(ModalCrearEditarBodegaInventarioComponent,
      {
        data: {
          bodegaInventario: bodegaInventario,
          idDepartamento: this.idDepartamento
        }
      });
    modalCargarArchivo.afterClosed()
      .subscribe(
        r => this.cargarDatos()
      )
  }

  reiniciarArreglo() {
    this.inventarios = [];
    this.totalBodegaInventario = 0;
  }


  activarDesactivarRegistro(bodegaInventario: any) {
    this._cargandoService.habilitarCargando();
    const indice = encontrarIndice(this.inventarios, bodegaInventario);
    const departamentoActualizar = {
      habilitado : !bodegaInventario.habilitado
    }
    this._bodegaInventarioRestService
      .editar(departamentoActualizar, bodegaInventario.id as number)
      .subscribe(
        r => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.success('Actualizado correctament', 'Correcto');
          this.inventarios[indice].habilitado = !bodegaInventario.habilitado;
        },
        error => {
          this._toastrService.error('Ocurrió un error', 'Error')
          this._cargandoService.deshabilitarCargando();
        }
      )
  }

  navegarArticulos(bodegaInventario: any) {
    this._router.navigate(['/inventarios', bodegaInventario.id, 'articulos-inventario'])
  }
}

