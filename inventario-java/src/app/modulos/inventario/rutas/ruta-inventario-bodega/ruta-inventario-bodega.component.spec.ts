import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaInventarioBodegaComponent } from './ruta-inventario-bodega.component';

describe('RutaInventarioBodegaComponent', () => {
  let component: RutaInventarioBodegaComponent;
  let fixture: ComponentFixture<RutaInventarioBodegaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RutaInventarioBodegaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaInventarioBodegaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
