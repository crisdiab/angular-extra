import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaMantenimientoInventarioComponent } from './ruta-mantenimiento-inventario.component';

describe('RutaMantenimientoInventarioComponent', () => {
  let component: RutaMantenimientoInventarioComponent;
  let fixture: ComponentFixture<RutaMantenimientoInventarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RutaMantenimientoInventarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaMantenimientoInventarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
