import {Component, OnInit} from '@angular/core';
import {TABLAS} from '../../../../constantes/tablas';
import {MatDialog} from '@angular/material/dialog';
import {MantenimientoInterface} from '../../interfaces/mantenimiento.interface';
import {ModalNuevoMantenimientoComponent} from '../../componentes/modal-nuevo-mantenimiento/modal-nuevo-mantenimiento.component';
import {MantenimientoRestService} from '../../../../servicios/rest/mantenimiento-rest.service';
import {ActivatedRoute} from '@angular/router';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {PAGINACION} from '../../../../constantes/skip-take';
import {BusquedaMantenimiento} from '../../interfaces/busqueda-mantenimiento';
import {encontrarIndice} from '../../../../funciones/encontrar-indice';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-ruta-mantenimiento-inventario',
  templateUrl: './ruta-mantenimiento-inventario.component.html',
  styleUrls: ['./ruta-mantenimiento-inventario.component.scss']
})
export class RutaMantenimientoInventarioComponent implements OnInit {


  columnasPrimeTable = [
    {field: 'nombre', header: 'Nombre', width: '70'},
    {field: 'id', header: 'Acciones', width: '30'}];
  filas = TABLAS.rows;
  mantenimientos: MantenimientoInterface[] = [];
  totalMantenimientos = 0;
  idArticuloInventario?: number;
  textoBusqueda?: string;
  estadoACambiarSeleccionado?: 'PR' | 'ER' | 'PE' | 'EN' | 'NR';
  busquedaEstado: any;

  constructor(
    private readonly _dialog: MatDialog,
    private readonly _mantenimientoRestService: MantenimientoRestService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService
  ) {
    moment.locale('es');
  }

  ngOnInit(): void {
    this.recuperarParametros();
  }

  recuperarParametros() {
    this._activatedRoute
      .params
      .subscribe(
        parametros => {
          this.idArticuloInventario = Number(parametros.idArticuloInventario);
          this.cargarDatos();
        }
      );
  }


  cargarDatos(skip?: number, take?: number) {
    this._cargandoService.habilitarCargando();
    let busqueda: BusquedaMantenimiento = {
      skip: skip ? skip : 0,
      take: take ? take : PAGINACION.take,
      busqueda: '',
      articuloInvId: this.idArticuloInventario as number
    };
    if (this.textoBusqueda !== '' && this.textoBusqueda) {
      busqueda.busqueda = this.textoBusqueda;
    }
    if (this.busquedaEstado) {
      busqueda.estado = this.busquedaEstado;
    }
    this._mantenimientoRestService.buscar(busqueda)
      .subscribe(
        (datos: any) => {
          this._cargandoService.deshabilitarCargando();
          this.mantenimientos = datos.listaObjetos.map(
            (r: MantenimientoInterface) => {
              if (r.fechaIniciaMantenimiento) {
              r.fechaIniciaMantenimiento = moment(r.fechaIniciaMantenimiento).add(1,'days').format('DD-MM-yyyy')
              }
              if (r.fechaEntrega) {
                r.fechaEntrega = moment(r.fechaEntrega).add(1,'days').format('DD-MM-yyyy')
              }
              if (r.fechaTerminaMantenimiento) {
                r.fechaTerminaMantenimiento = moment(r.fechaTerminaMantenimiento).add(1,'days').format('DD-MM-yyyy')
              }
              return r;
            }
          );
          this.totalMantenimientos = datos.cantidadRegistros;
        },
        error => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.error('Ocurrió un error', 'Error');
        }
      );


  }

  cargarMasDatos(evento: any) {
    this.cargarDatos(evento.first, evento.rows);
  }

  abrirModalCrearEditar(mantenimiento?: MantenimientoInterface | undefined) {
    const modalCargarArchivo = this._dialog.open(ModalNuevoMantenimientoComponent,
      {
        data: {
          mantenimiento: mantenimiento,
          idArticuloInventario: this.idArticuloInventario
        }
      });
    modalCargarArchivo.afterClosed()
      .subscribe(
        r => {
          this.cargarDatos();
        }
      );
  }

  reiniciarArreglo() {
    this.mantenimientos = [];
    this.totalMantenimientos = 0;
  }

  actualizarMantenimiento(mantenimiento: MantenimientoInterface): void {
    if (this.estadoACambiarSeleccionado) {
      this._cargandoService.habilitarCargando();
      const indice = encontrarIndice(this.mantenimientos, mantenimiento);
      let actualizarMantenimiento: MantenimientoInterface = {};

      switch (this.estadoACambiarSeleccionado) {
        case 'EN':
          actualizarMantenimiento = {
            estado: 'EN',
            fechaEntrega: moment().tz("America/Guayaquil").format('yyyy-MM-DD'),
            fechaTerminaMantenimiento: moment().tz("America/Guayaquil").format('yyyy-MM-DD'),
            id: mantenimiento.id
          };
          break;
        case 'ER':
          actualizarMantenimiento = {
            estado: 'ER',
            fechaIniciaMantenimiento: moment().tz("America/Guayaquil").format('yyyy-MM-DD'),
            id: mantenimiento.id
          };
          break;
        case 'PE':
          actualizarMantenimiento = {
            estado: 'PE',
            fechaTerminaMantenimiento: moment().tz("America/Guayaquil").format('yyyy-MM-DD'),
            id: mantenimiento.id
          };
          break;
        case 'NR':
          actualizarMantenimiento = {
            estado: 'NR',
            fechaTerminaMantenimiento: moment().tz("America/Guayaquil").format('yyyy-MM-DD'),
            id: mantenimiento.id
          };
          break;

      }

      console.log('actualizar matnenimiento', actualizarMantenimiento);
      this._mantenimientoRestService
        .editar(actualizarMantenimiento, mantenimiento.id as number)
        .subscribe(
          r => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Actualizado correctamente', 'Correcto');
            this.mantenimientos[indice].estado = this.estadoACambiarSeleccionado;
            this.cargarDatos();
          },
          (error) => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
            console.error('error', error);
          }
        );
    }


  }
}


