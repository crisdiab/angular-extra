import {Component, OnInit} from '@angular/core';
import {ModalCrearEditarBodegaInventarioComponent} from '../../componentes/modal-crear-editar-bodega-inventario/modal-crear-editar-bodega-inventario.component';
import {ArticuloInventarioInterface} from '../../interfaces/articulo-inventario.interface';
import {ToastrService} from 'ngx-toastr';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {ArticuloInventarioRestService} from '../../../../servicios/rest/articulo-inventario-rest.service';
import {TABLAS} from '../../../../constantes/tablas';
import {ModalAgregarProductoInventarioComponent} from '../../componentes/modal-agregar-producto-inventario/modal-agregar-producto-inventario.component';
import {ModalIngresoEgresoComponent} from '../../componentes/modal-ingreso-egreso/modal-ingreso-egreso.component';
import {BusquedaDepartamentos} from '../../../departamento/interfaces/busqueda-departamentos';
import {PAGINACION} from '../../../../constantes/skip-take';
import {BusquedaArticulosInventario} from '../../interfaces/busqueda-articulos-inventario';
import {encontrarIndice} from '../../../../funciones/encontrar-indice';
import {BodegaInterface} from '../../../bodega/interfaces/bodega.interface';
import {BodegaInventarioRestService} from '../../../../servicios/rest/bodega-inventario-rest.service';

@Component({
  selector: 'app-ruta-articulo-inventario',
  templateUrl: './ruta-articulo-inventario.component.html',
  styleUrls: ['./ruta-articulo-inventario.component.scss']
})
export class RutaArticuloInventarioComponent implements OnInit {

  idBodegaInventario?: number;
  columnasPrimeTable = [
    {field: 'stock', header: 'Nombre', width: '70'},
    {field: 'id', header: 'Acciones', width: '30'}];
  filas = TABLAS.rows;
  articulosInventario: ArticuloInventarioInterface[] = [];
  totalArticulosInventario: number = 0;
  textoBusqueda?: string;
  bodegaInventarioMantenimiento?: { nombre?: string, descripcion?: string, esMantenimiento?: boolean };

  constructor(
    private readonly _dialog: MatDialog,
    private readonly _toastrService: ToastrService,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _articuloInventarioRestService: ArticuloInventarioRestService,
    private readonly _bodegaInventarioRestService: BodegaInventarioRestService,
    private readonly _router: Router
  ) {
  }

  ngOnInit(): void {
    this.recuperarParametros();
  }

  recuperarParametros() {
    this._activatedRoute
      .params
      .subscribe(
        parametros => {
          this.idBodegaInventario = parametros.idBodegaInventario;
          this.cargarDatos();
          this._bodegaInventarioRestService.bodegaInventarioPorId(Number(this.idBodegaInventario))
            .subscribe(
              r => {
                console.log('r', r)
                this.bodegaInventarioMantenimiento = r;
              }
            )
        }
      );
  }

  cargarDatos(skip?: number, take?: number) {
    let busqueda: BusquedaArticulosInventario = {
      skip: skip ? skip : 0,
      take: take ? take : PAGINACION.take,
      busqueda: '',
      idBodegaInventario: Number(this.idBodegaInventario)
    };
    if (this.textoBusqueda !== '' && this.textoBusqueda) {
      busqueda.busqueda = this.textoBusqueda;
    }
    this._articuloInventarioRestService.buscar(busqueda)
      .subscribe(
        (datos: any) => {
          this.articulosInventario = datos.listaObjetos
          this.totalArticulosInventario = this.textoBusqueda !== '' ? datos.cantidadRegistros : this.articulosInventario.length;
        },
        (error: any) => {
          console.error('error', error);
          this._toastrService.error('Ocurrió un error al cargar artìculos', 'Error');
        }
      );

  }

  cargarMasDatos(evento: any) {
    this.cargarDatos(evento.first, evento.rows);
  }

  abrirModalCrearEditar(articuloInventario?: ArticuloInventarioInterface | undefined) {
    // const modalCargarArchivo = this._dialog.open(ModalAgregarProductoInventarioComponent,
    const modalCargarArchivo = this._dialog.open(ModalAgregarProductoInventarioComponent,
      {
        data: {
          articuloInventario: articuloInventario,
          idBodegaInventario: this.idBodegaInventario,
          tipoBodega: this.bodegaInventarioMantenimiento,
        }
      });
    if (articuloInventario) {
      const indice = this.articulosInventario.findIndex(
        (elemento) => elemento.id === articuloInventario.id
      );
      modalCargarArchivo.afterClosed()
        .subscribe(
          r => {
            this.cargarDatos();
          }
        );
    } else {
      modalCargarArchivo.afterClosed()
        .subscribe(
          r => {
            this.cargarDatos();
          }
        )
      ;
    }
  }

  abrirModalIngresoEgreso(articuloInventario: ArticuloInventarioInterface) {
    const modalIngresoEgreso = this._dialog.open(ModalIngresoEgresoComponent,
      {
        width: '400px',
        data: {
          articuloInventario: articuloInventario,
        }
      });
    const indice = this.articulosInventario.findIndex(
      (elemento) => {
        return elemento.id === articuloInventario.id;
      }
    );
    modalIngresoEgreso.afterClosed()
      .subscribe(
        resultado => {
          // if (resultado.cantidad) {
          //   this.articulosInventario[indice].stock = resultado.cantidad;
          // }
          this.cargarDatos();
        }
      );
  }

  reiniciarArreglo() {
    this.articulosInventario = [];
    this.totalArticulosInventario = 0;
  }

  navegarMantenimiento(articuloInventario: any) {
    this._router.navigate(['/inventarios', articuloInventario.id, 'mantenimiento']);
  }

}
