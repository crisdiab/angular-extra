import {Component, OnInit} from '@angular/core';
import {TABLAS} from '../../../../constantes/tablas';
import {MatDialog} from '@angular/material/dialog';
import {UsuarioInterface} from '../../interfaces/usuario.interface';
import {ModalCrearEditarUsuarioComponent} from '../../componentes/modal-crear-editar-usuario/modal-crear-editar-usuario.component';
import {UsuarioRestService} from '../../../../servicios/rest/usuario-rest.service';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {encontrarIndice} from '../../../../funciones/encontrar-indice';
import {PAGINACION} from '../../../../constantes/skip-take';
import {BusquedaUsuarioInterface} from '../../../departamento/interfaces/busqueda-usuario.interface';

@Component({
  selector: 'app-ruta-gestion-usuarios',
  templateUrl: './ruta-gestion-usuarios.component.html',
  styleUrls: ['./ruta-gestion-usuarios.component.scss']
})
export class RutaGestionUsuariosComponent implements OnInit {
  columnasPrimeTable = [
    {field: 'nombre', header: 'Nombre', width: '70'},
    {field: 'id', header: 'Acciones', width: '30'}];
  filas = TABLAS.rows;
  usuarios?: UsuarioInterface[] = [];
  totalUsuarios = 0;
  textoBusqueda?: string;


  constructor(
    private readonly _dialog: MatDialog,
    private readonly _usuarioRestService: UsuarioRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.cargarDatos();
  }

  limpiarFiltroGeneral() {

  }


  cargarDatos(skip?: number, take?: number) {
    this._cargandoService.habilitarCargando();
    let busqueda: BusquedaUsuarioInterface = {
      skip: skip ? skip : 0,
      take: take ? take : PAGINACION.take,
      busqueda: ''
    };
    if (this.textoBusqueda !== '' && this.textoBusqueda) {
      busqueda.busqueda = this.textoBusqueda;
    }
    this._usuarioRestService.buscarUsuarios(busqueda)
      .subscribe(
        (datos: any) => {
          this.usuarios = datos.listaObjetos;
          this.totalUsuarios = this.textoBusqueda !== '' ? datos.cantidadRegistros : this.usuarios?.length;
          setTimeout(() => {
            this._cargandoService.deshabilitarCargando();
          }, 1000);


        },
        error => {
          this._cargandoService.deshabilitarCargando();
          this._toastrService.error('Ocurrió un error', 'Error');
          console.error(error);
        }
      );


  }

  cargarMasDatos(evento: any) {
    this.cargarDatos(evento.first, evento.rows);
  }

  abrirModalCrearEditar(usuario?: UsuarioInterface | undefined) {
    const modalCargarArchivo = this._dialog.open(ModalCrearEditarUsuarioComponent,
      {
        data: {
          usuario: usuario
        }
      });
    modalCargarArchivo.afterClosed()
      .subscribe(
        r => this.cargarDatos()
      );
  }

  reiniciarArreglo() {
    this.usuarios = [];
    this.totalUsuarios = 0;
  }

  activarDesactivarRegistro(usuario: UsuarioInterface) {
    if (this.usuarios) {
      this._cargandoService.habilitarCargando();
      const indice = encontrarIndice(this.usuarios, usuario);
      const usuarioActualizar = {
        habilitado: !usuario.habilitado,
        idUsuario: usuario.id
      };
      this._usuarioRestService
        .editar(usuarioActualizar, usuario.id as number)
        .subscribe(
          r => {
            this._toastrService.success('Actualizado correctament', 'Correcto');
            // @ts-ignore
            const elemento = this.usuarios[indice];
            elemento.habilitado = !usuario.habilitado;
            setTimeout(() => {
              this._cargandoService.deshabilitarCargando();
            }, 1000);
          },
          error => {
            this._toastrService.error('Ocurrió un error', 'Error');
            setTimeout(() => {
              this._cargandoService.deshabilitarCargando();
            }, 1000);
          }
        );
    }

  }

  actualizarPermisos(usuario: UsuarioInterface, permiso: 'GP' | 'GB' | 'GD' | 'GI' | 'M' | 'VR'| 'GU'): void {
    let actualizar: UsuarioInterface;
    switch (permiso) {
      case 'GB':
        // actualizar = {
        //   gestionaBodegas: !usuario.gestionaBodegas,
        //   id: usuario.id,
        //   daMantenimiento: usuario.daMantenimiento,
        //   gestionaProductos: usuario.gestionaProductos,
        //   nombre: usuario.nombre,
        //   habilitado: usuario.habilitado,
        //   cedula:usuario.cedula,
        //   password
        // };
        usuario.gestionaBodegas = !usuario.gestionaBodegas;
        break;
      case 'GD':
        usuario.gestionaDepartamentos = !usuario.gestionaDepartamentos;
        break;
      case 'GI':
      usuario.gestionaInventario = !usuario.gestionaInventario;
        break;
      case 'GP':
        usuario.gestionaProductos = !usuario.gestionaProductos;
        break;
      case 'M':
        usuario.daMantenimiento = !usuario.daMantenimiento;
        break;
      case 'VR':
        usuario.verReporte = !usuario.verReporte;
        break;
      case 'GU':
        // @ts-ignore
        usuario.gestionaUsuarios = !usuario.gestionaUsuarios;
        break;
    }

    if (this.usuarios) {
      this._cargandoService.habilitarCargando();
      const indice = encontrarIndice(this.usuarios, usuario);
      this._usuarioRestService
        .editar(usuario, usuario.id as number)
        .subscribe(
          r => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Actualizado correctamente', 'Correcto');
            switch (permiso) {
              case 'GB':
                // @ts-ignore
                this.usuarios[indice].gestionaBodegas = usuario.gestionaBodegas;
                break;
              case 'GD':
                // @ts-ignore
                this.usuarios[indice].gestionaDepartamentos = usuario.gestionaDepartamentos;
                break;
              case 'GI':
                // @ts-ignore
                this.usuarios[indice].gestionaInventario = usuario.gestionaInventario;
                break;
              case 'GP':
                // @ts-ignore
                this.usuarios[indice].gestionaProductos = usuario.gestionaProductos;
                break;
              case 'M':
                // @ts-ignore
                this.usuarios[indice].daMantenimiento = usuario.daMantenimiento;
                break;
              case 'VR':
                // @ts-ignore
                this.usuarios[indice].verReportes = usuario.verReporte;
                break;
              case 'GU':
                // @ts-ignore
                this.usuarios[indice].gestionaUsuarios = usuario.gestionaUsuarios;
                break;
            }
          },
          error => {
            console.error(error);
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        );
    }

  }
}
