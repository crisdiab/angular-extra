import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { RutaGestionUsuariosComponent } from './rutas/ruta-gestion-usuarios/ruta-gestion-usuarios.component';
import { ModalCrearEditarUsuarioComponent } from './componentes/modal-crear-editar-usuario/modal-crear-editar-usuario.component';
import { FormularioUsuarioComponent } from './componentes/formulario-usuario/formulario-usuario.component';
import {MatCardModule} from '@angular/material/card';
import {TituloRutaModule} from '../../componentes/titulo-ruta/titulo-ruta.module';
import {TableModule} from 'primeng/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';
import {ToolbarModule} from 'primeng/toolbar';
import {MatDialogModule} from '@angular/material/dialog';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MODULOS_COMUNES} from '../../constantes/modulos-comunes';


@NgModule({
  declarations: [RutaGestionUsuariosComponent, ModalCrearEditarUsuarioComponent, FormularioUsuarioComponent],
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    TituloRutaModule,
    ReactiveFormsModule,
    ...MODULOS_COMUNES
  ]
})
export class UsuarioModule { }
