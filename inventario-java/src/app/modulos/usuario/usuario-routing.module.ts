import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaGestionUsuariosComponent} from './rutas/ruta-gestion-usuarios/ruta-gestion-usuarios.component';

const routes: Routes = [
  {
    path: 'gestion',
    component: RutaGestionUsuariosComponent
  },
  {
    path: '',
    redirectTo: 'gestion'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
