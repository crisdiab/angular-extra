import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarUsuarioComponent } from './modal-crear-editar-usuario.component';

describe('ModalCrearEditarUsuarioComponent', () => {
  let component: ModalCrearEditarUsuarioComponent;
  let fixture: ComponentFixture<ModalCrearEditarUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarUsuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
