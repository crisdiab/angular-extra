import {Component, Inject, OnInit} from '@angular/core';
import {ProductoInterface} from '../../../producto/interfaces/producto.interface';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {ProductoRestService} from '../../../../servicios/rest/producto-rest.service';
import {CargandoService} from '../../../../servicios/cargando.service';
import {ToastrService} from 'ngx-toastr';
import {UsuarioInterface} from '../../interfaces/usuario.interface';
import {UsuarioRestService} from '../../../../servicios/rest/usuario-rest.service';
import {BodegaInterface} from '../../../bodega/interfaces/bodega.interface';

@Component({
  selector: 'app-modal-crear-editar-usuario',
  templateUrl: './modal-crear-editar-usuario.component.html',
  styleUrls: ['./modal-crear-editar-usuario.component.scss']
})
export class ModalCrearEditarUsuarioComponent implements OnInit {

  usuarioACrear?: UsuarioInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { usuario: UsuarioInterface | undefined },
    private readonly _usuarioRestService: UsuarioRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _toastrService: ToastrService,
    private readonly _dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
  }

  guardarRegistro() {
    this._cargandoService.habilitarCargando();
    if (this.data.usuario) {
      // @ts-ignore
      this.usuarioACrear.id = this.data.usuario.id;
      // @ts-ignore
      this.usuarioACrear?.habilitado = this.data.usuario.habilitado;
      this._usuarioRestService
        .editar(this.usuarioACrear as BodegaInterface, this.data.usuario.id as number)
        .subscribe(
          creado => {
            this._cargandoService.deshabilitarCargando();
            this._toastrService.success('Se actualizó correctamente');
            this._dialog.closeAll();
          },
          error => {
            console.error({
              error
            });
            this._cargandoService.deshabilitarCargando();
            this._toastrService.error('Ocurrió un error', 'Error');
          }
        )
    } else {
      if(this.usuarioACrear) {
        this.usuarioACrear.habilitado = true;
        this._usuarioRestService
          .crear(this.usuarioACrear as UsuarioInterface)
          .subscribe(
            creado => {
              this._cargandoService.deshabilitarCargando();
              this._toastrService.success('Se creó correctamente');
              this._dialog.closeAll();
            },
            error => {
              console.error({
                error
              });
              this._cargandoService.deshabilitarCargando();
              this._toastrService.error('Ocurrió un error', 'Error');
            }
          );
      }
    }
  }
  setearUsuario(usuario: UsuarioInterface | undefined) {
    this.usuarioACrear = usuario;
  }

}
