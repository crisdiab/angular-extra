import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  MENSAJES_VALIDACION_CEDULA,
  MENSAJES_VALIDACION_NOMBRE, MENSAJES_VALIDACION_PASSWORD, MENSAJES_VALIDACION_USUARIO
} from '../../../bodega/constantes/mensajes-validacion';
import {Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {UsuarioInterface} from '../../interfaces/usuario.interface';
import {ToastrService} from 'ngx-toastr';
import {validarCedula} from '../validar-cedula';

@Component({
  selector: 'app-formulario-usuario',
  templateUrl: './formulario-usuario.component.html',
  styleUrls: ['./formulario-usuario.component.scss']
})
export class FormularioUsuarioComponent implements OnInit {

  hide = true;
  formularioUsuario?: FormGroup;
  formularioValido = false;
  @Input() usuario?: UsuarioInterface;
  mensajesErrorNombre: string[] = [];
  mensajesErrorCedula: string[] = [];
  mensajesErrorUsuario: string[] = [];
  mensajesErrorPassword: string[] = [];

  private _mensajesValidacionNombre = MENSAJES_VALIDACION_NOMBRE;
  private _mensajesValidacionCedula = MENSAJES_VALIDACION_CEDULA;
  private _mensajesValidacionUsuario = MENSAJES_VALIDACION_USUARIO;
  private _mensajesValidacionPassword = MENSAJES_VALIDACION_PASSWORD;

  subscriptores: Subscription[] = [];
  ocultarPass = false;
  @Output() formularioEmitter: EventEmitter<UsuarioInterface | undefined> = new EventEmitter<UsuarioInterface | undefined>();

  constructor(private readonly _formBuilder: FormBuilder,
              private readonly _toasterService: ToastrService) {

  }

  ngOnInit(): void {

    this.iniciarFormulario();
    this._escucharFormulario();
    this._escucharCampoNombre();
    this._escucharCampoCedula();
    this._escucharCampoUsuario();
    this._escucharCampoPassword();

  }

  ngOnDestroy(): void {
    this.subscriptores.forEach(valor => valor.unsubscribe());
  }

  iniciarFormulario() {
    this.formularioUsuario = this._formBuilder.group(
      {
        nombre: [this.usuario ? this.usuario.nombre : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(80),
            Validators.pattern(/[A-Za-z]+/g)
          ],
        ],
        cedula: [this.usuario ? this.usuario.cedula : '',
          [
            Validators.required,
            Validators.minLength(10),
            Validators.maxLength(10),
            Validators.pattern(/^-?(0|[1-9]\d*)?$/)
          ],
        ],
        usuario: [this.usuario ? this.usuario.usuario : '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ],
        ],
        password: [this.usuario ? this.usuario.password : '',
          [],
        ],
      });
    if (this.usuario) {
      this.ocultarPass = true;
    } else {
      this.formularioUsuario.get('password')?.setValidators(
        [
          Validators.required,
          Validators.minLength(8),
        ]);
    }
  }

  private _escucharFormulario() {
    // el observable que escucha cambios
    if (this.formularioUsuario) {

      const observableCambiosFormulario$ = this.formularioUsuario.valueChanges;
      const observableFOmrulario = observableCambiosFormulario$
        .subscribe((formulario) => {
          if (this.formularioUsuario?.valid) {
            if(validarCedula(formulario.cedula)){
              this.formularioEmitter.emit(formulario);
            }else {
              const controCedula = this.formularioUsuario?.get('cedula') as AbstractControl;
              controCedula.reset();
              this._toasterService.error('La cédula no es válida', 'Error');
              this.formularioEmitter.emit(undefined);

            }

          } else {
            this.formularioEmitter.emit(undefined);
          }
        });
      this.subscriptores.push(observableFOmrulario);
    }
  }

  private _escucharCampoNombre() {
    const controlNombre = this.formularioUsuario?.get('nombre') as AbstractControl;

    const observableDeCambiosNombre$ = controlNombre.valueChanges;

    const respuestaObservableNombre = observableDeCambiosNombre$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesNombre(controlNombre);
      });
    this.subscriptores.push(respuestaObservableNombre);
  }

  private _setearMensajesNombre(controlForm: AbstractControl) {
    this.mensajesErrorNombre = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorNombre = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' | 'pattern' = atributo as 'required' | 'minlength' | 'maxlength' | 'pattern';
          return this._mensajesValidacionNombre[mensajeError];
        });
    }
  }

  private _escucharCampoCedula() {
    const controCedula = this.formularioUsuario?.get('cedula') as AbstractControl;

    const observableDeCambiosCedula$ = controCedula.valueChanges;

    const respuestaObservableCedula = observableDeCambiosCedula$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {


        if (validarCedula(respuesta.toString())) {
          this._setearMensajesCedula(controCedula);
          this._toasterService.success('Cédula válida');
        } else {
          controCedula.reset();
          this._toasterService.error('La cédula no es válida', 'Error');
        }

      });
    this.subscriptores.push(respuestaObservableCedula);
  }

  private _setearMensajesCedula(controlForm: AbstractControl) {
    this.mensajesErrorCedula = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorCedula = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength';
          return this._mensajesValidacionCedula[mensajeError];
        });
    }
  }

  private _escucharCampoUsuario() {
    const controlUsuario = this.formularioUsuario?.get('usuario') as AbstractControl;

    const observableDeCambiosUsuario$ = controlUsuario.valueChanges;

    const respuestaObservableUsuario = observableDeCambiosUsuario$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesUsuario(controlUsuario);
      });
    this.subscriptores.push(respuestaObservableUsuario);
  }

  private _setearMensajesUsuario(controlForm: AbstractControl) {
    this.mensajesErrorUsuario = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorUsuario = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength';
          return this._mensajesValidacionUsuario[mensajeError];
        });
    }
  }

  private _escucharCampoPassword() {
    const controlPassword = this.formularioUsuario?.get('password') as AbstractControl;

    const observableDeCambiosPassword$ = controlPassword.valueChanges;

    const respuestaObservablePassword = observableDeCambiosPassword$
      .pipe(
        debounceTime(1000)
      )
      .subscribe((respuesta) => {
        this._setearMensajesPassword(controlPassword);
      });
    this.subscriptores.push(respuestaObservablePassword);
  }

  private _setearMensajesPassword(controlForm: AbstractControl) {
    this.mensajesErrorPassword = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorPassword = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' = atributo as 'required';
          return this._mensajesValidacionPassword[mensajeError];
        });
    }
  }

}
