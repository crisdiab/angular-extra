export interface UsuarioInterface {
  id?: number;
  cedula?: string;
  nombre?: string;
  usuario?: string;
  password?: string;
  habilitado?: boolean;
  gestionaProductos?: boolean;
  gestionaBodegas?: boolean;
  gestionaDepartamentos?: boolean;
  gestionaInventario?: boolean;
  daMantenimiento?: boolean;
  verReporte?: boolean;
}
