import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {convertirQueryParams} from '../funciones/convertir-query-params';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = environment.url + '/usuarios/login';
  estaLogeado = false;

  constructor(
    private readonly _httpClient: HttpClient,
    private readonly _router: Router
  ) {
  }

  login(credenciales: { usuario: string, password: string }) {
    const parametros = convertirQueryParams(credenciales)
    return this._httpClient
      .get(this.url+  parametros);
  }

  logout() {
    this.estaLogeado = false;
    localStorage.removeItem('usuario');
    this._router.navigate(['/login']);

  }

  guardarDatos(usuario: UsuarioLogin) {
    console.log('usuario a guardar',usuario)
    localStorage.setItem('usuario', JSON.stringify(usuario));
    this.estaLogeado = true;
    console.log('se guardo')
  }

  recuperarDatos(): UsuarioLogin {
    const objetoUsuario = localStorage.getItem('usuario') as string;
    console.log('recuperar', JSON.parse(objetoUsuario) )
    return JSON.parse(objetoUsuario);
  }
}

export interface UsuarioLogin {
  id?: number;
  gestionaProductos?: boolean;
  gestionaBodegas?: boolean;
  gestionaDepartamentos?: boolean;
  gestionaInventario?: boolean;
  daMantenimiento?: boolean;
  verReporte?: boolean;
  gestionaUsuarios?: boolean;
}
