import {HttpClient} from '@angular/common/http';
import {DepartamentoInterface} from '../modulos/departamento/interfaces/departamento.interface';
import {convertirQueryParams} from '../funciones/convertir-query-params';

export class ServicioGenerico<Entidad> {


  constructor(
    private readonly httpClient: HttpClient,
    public readonly url: string,
    public readonly path: string
  ) {
  }

  crear(departamento: Entidad) {
    return this.httpClient.post(this.url + this.path, departamento);
  }

  editar(departamento: Entidad, idDepartamento: number) {
    return this.httpClient.put(this.url + this.path , departamento);
  }

  buscar(parametros?: any) {
    const consulta = convertirQueryParams(parametros);
    const urlConsulta = `${this.url}${this.path}${consulta}`;
    return this.httpClient.get(urlConsulta);
  }

  eliminar(idDepartamento: number) {
    return this.httpClient.delete(this.url + this.path + '/' + idDepartamento);
  }

}
