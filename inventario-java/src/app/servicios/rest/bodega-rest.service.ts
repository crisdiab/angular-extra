import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BodegaInterface} from '../../modulos/bodega/interfaces/bodega.interface';
import {ServicioGenerico} from '../servicio-generico';

@Injectable({
  providedIn: 'root'
})
export class BodegaRestService extends ServicioGenerico<BodegaInterface>{

  constructor(
    private readonly _httpClient: HttpClient
  ) {
    super(
      _httpClient,
      environment.url,
      '/tipo-bodega'
    )
  }

}
