import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {DepartamentoInterface} from '../../modulos/departamento/interfaces/departamento.interface';
import {convertirQueryParams} from '../../funciones/convertir-query-params';
import {ServicioGenerico} from '../servicio-generico';

@Injectable({
  providedIn: 'root'
})
export class DepartamentoRestService extends ServicioGenerico<DepartamentoInterface> {

  constructor(
    private readonly _httpClient: HttpClient
  ) {

    super(
      _httpClient,
      environment.url,
      '/departamentos');
  }

}
