import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {MantenimientoInterface} from '../../modulos/inventario/interfaces/mantenimiento.interface';
import {BodegaInterface} from '../../modulos/bodega/interfaces/bodega.interface';
import {ProductoInterface} from '../../modulos/producto/interfaces/producto.interface';
import {ServicioGenerico} from '../servicio-generico';

@Injectable({
  providedIn: 'root'
})
export class ProductoRestService extends ServicioGenerico<ProductoInterface>{

  constructor(
    private readonly _httpClient: HttpClient
  ) {
    super(
      _httpClient,
      environment.url,
      '/productos'
    )
  }

}
