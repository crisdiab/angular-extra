import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BodegaInterface} from '../../modulos/bodega/interfaces/bodega.interface';
import {MantenimientoInterface} from '../../modulos/inventario/interfaces/mantenimiento.interface';
import {ServicioGenerico} from '../servicio-generico';
import {convertirQueryParams} from '../../funciones/convertir-query-params';
import {ReporteConsultaInterface} from '../../modulos/reportes/rutas/ruta-reporte/ruta-reporte.component';

@Injectable({
  providedIn: 'root'
})
export class MantenimientoRestService extends ServicioGenerico<MantenimientoInterface> {

  url = environment.url;
  path = '/mantenimiento';

  constructor(
    private readonly _httpClient: HttpClient
  ) {
    super(
      _httpClient,
      environment.url,
      '/mantenimiento'
    );
  }

  generarReporteEstado(parametros: ReporteConsultaInterface) {
    const parametrosUrl = convertirQueryParams(parametros);
    return this._httpClient.get(`${this.url}${this.path}/reporteEstado${parametrosUrl}`);
  }

  generarReporteCedula(parametros: ReporteConsultaInterface) {
    const parametrosUrl = convertirQueryParams(parametros);
    return this._httpClient.get(`${this.url}${this.path}/reporteCedula${parametrosUrl}`);
  }

  generarReporteArticuloInventario(parametros: ReporteConsultaInterface) {
    const parametrosUrl = convertirQueryParams(parametros);
    return this._httpClient.get(`${this.url}${this.path}/reporteArticuloInv${parametrosUrl}`);
  }


}
