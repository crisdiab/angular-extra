import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BodegaInventarioInterface} from '../../modulos/inventario/interfaces/bodega-inventario.interface';
import {ArticuloInventarioInterface} from "../../modulos/inventario/interfaces/articulo-inventario.interface";
import {KardexInterface} from '../../modulos/inventario/interfaces/kardex.interface';
import {ServicioGenerico} from '../servicio-generico';
import {convertirQueryParams} from '../../funciones/convertir-query-params';
import {ReporteConsultaInterface} from '../../modulos/reportes/rutas/ruta-reporte/ruta-reporte.component';


@Injectable({
  providedIn: 'root'
})
export class ArticuloInventarioRestService extends ServicioGenerico<ArticuloInventarioInterface>{

  constructor(
    private readonly _httpClient: HttpClient
  ) {
    super(
      _httpClient,
      environment.url,
      '/articulo-inventario'
    )
  }

  ingresoEgreso(registro:
                 KardexInterface) {
    const urlEnvio = `${this.url}/kardex`
    return this._httpClient.post(urlEnvio, registro)
  }

  generarReporteIngresosEgresos(parametros: ReporteConsultaInterface) {
    const parametrosUrl = convertirQueryParams(parametros);
    return this._httpClient.get(`${this.url}/kardex${parametrosUrl}` );
  }

  // generarReporteArticuloInv(parametros:ReporteConsultaInterface) {
  //   const parametrosUrl = convertirQueryParams(parametros);
  //   return this._httpClient.get(`${this.url}/kardex${parametrosUrl}` );
  // }
}
