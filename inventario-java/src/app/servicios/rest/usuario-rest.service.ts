import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ProductoInterface} from '../../modulos/producto/interfaces/producto.interface';
import {UsuarioInterface} from '../../modulos/usuario/interfaces/usuario.interface';
import {Observable} from 'rxjs';
import {ServicioGenerico} from '../servicio-generico';
import {convertirQueryParams} from '../../funciones/convertir-query-params';

@Injectable({
  providedIn: 'root'
})
export class UsuarioRestService extends ServicioGenerico<UsuarioInterface>{

  constructor(
    private readonly _httpClient: HttpClient
  ) {
    super(
      _httpClient,
      environment.url,
      '/usuarios'
    )
  }

  buscarUsuarios(parametros?: any) {
    const consulta = convertirQueryParams(parametros);
    const urlConsulta = `${this.url}${this.path}/listar${consulta}`;
    return this._httpClient.get(urlConsulta);
  }

}
