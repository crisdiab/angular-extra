import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BodegaInventarioInterface} from '../../modulos/inventario/interfaces/bodega-inventario.interface';
import {ServicioGenerico} from '../servicio-generico';


@Injectable({
  providedIn: 'root'
})
export class BodegaInventarioRestService extends ServicioGenerico<BodegaInventarioInterface> {

  constructor(
    private readonly _httpClient: HttpClient
  ) {
    super(
      _httpClient,
      environment.url,
      '/bodega-inventario'
    );
  }

  bodegaInventarioPorId(idBodegaInventario: number){

    const urlEnvio = environment.url + '/bodega-inventario/bodegaInventarioPorId?idBodegaInventario='+ idBodegaInventario
    return this._httpClient.get(urlEnvio);
  }

}
