import {Injectable} from '@angular/core';
import {LoginService} from '../login.service';
import {CanActivate, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class GestionaBodegasGuard implements CanActivate {
  constructor(public _loginService: LoginService,
              private readonly _toastrService: ToastrService) {}
  canActivate(): boolean {
    const puedeGestionarBodegas = this._loginService.recuperarDatos().gestionaBodegas;

    if (puedeGestionarBodegas) {
      return true;
    }else {
      this._toastrService.warning('No tiene permisos para bodegas', 'Cuidado')
      return false;
    }

  }
}
