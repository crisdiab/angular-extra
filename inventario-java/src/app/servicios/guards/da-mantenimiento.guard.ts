import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {LoginService} from '../login.service';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class DaMantenimientoGuard implements CanActivate {
  constructor(public _loginService: LoginService,
              private readonly _toastrService: ToastrService) {}
  canActivate(): boolean {
    const puedDarMantenimiento = this._loginService.recuperarDatos().daMantenimiento;

    if (puedDarMantenimiento) {
      return true;
    }else {
      this._toastrService.warning('No tiene permisos para mantenimientos', 'Cuidado')
      return false;
    }

  }
}
