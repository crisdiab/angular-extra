import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {LoginService} from '../login.service';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class GestionaUsuariosGuard implements CanActivate {
  constructor(public _loginService: LoginService,
              private readonly _toastrService: ToastrService) {}
  canActivate(): boolean {
    const puedeGestionarUsuarios = this._loginService.recuperarDatos().gestionaUsuarios;
console.log('puedegestionar usuarios', puedeGestionarUsuarios);
    if (puedeGestionarUsuarios) {
      return true;
    }else {
      this._toastrService.warning('No tiene permisos para gestionar usuarios', 'Cuidado')
      return false;
    }
  }
}
