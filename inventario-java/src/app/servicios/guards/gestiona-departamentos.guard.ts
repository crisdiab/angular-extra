import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {LoginService} from '../login.service';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class GestionaDepartamentosGuard implements CanActivate {
  constructor(public _loginService: LoginService,
              private readonly _toastrService: ToastrService) {}
  canActivate(): boolean {
    const gestionaDepartamentos = this._loginService.recuperarDatos().gestionaDepartamentos;

    if (gestionaDepartamentos) {
      return true;
    }else {
      this._toastrService.warning('No tiene permisos para departamentos', 'Cuidado')
      return false;
    }

  }
}
