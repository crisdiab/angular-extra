import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {LoginService} from '../login.service';

@Injectable()
export class EstaLogeadoGuard implements CanActivate {
  constructor(public _loginService: LoginService, public router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log(this._loginService.estaLogeado)
    if (this._loginService.estaLogeado) {
      return true
    } else {
      return this.router.parseUrl('/login');
    }
  }

}
