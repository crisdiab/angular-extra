import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {LoginService} from '../login.service';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class GestionaInventariosGuard implements CanActivate {
  constructor(public _loginService: LoginService,
              private readonly _toastrService: ToastrService) {}
  canActivate(): boolean {
    const gestiotaInventarios = this._loginService.recuperarDatos().gestionaInventario;

    if (gestiotaInventarios) {
      return true;
    }else {
      this._toastrService.warning('No tiene permisos para inventarios', 'Cuidado')
      return false;
    }

  }
}
