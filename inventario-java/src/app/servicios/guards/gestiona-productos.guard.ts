import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {LoginService} from '../login.service';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class GestionaProductosGuard implements CanActivate {
  constructor(public _loginService: LoginService,
              private readonly _toastrService: ToastrService) {}
  canActivate(): boolean {
    const gestionaProductos = this._loginService.recuperarDatos().gestionaProductos;

    if (gestionaProductos) {
      return true;
    }else {
      this._toastrService.warning('No tiene permisos para productos', 'Cuidado')
      return false;
    }

  }
}
