import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {LoginService} from '../login.service';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class VeReportesGuard implements CanActivate {
  constructor(public _loginService: LoginService,
              private readonly _toastrService: ToastrService) {}
  canActivate(): boolean {
    console.log('va a culaer')
    const datos = this._loginService.recuperarDatos().verReporte;
    if (datos) {
      return true;
    }else {
      this._toastrService.warning('No tiene permisos para reportes', 'Cuidado')
      return false;
    }

  }
}
