import {Injectable, EventEmitter} from '@angular/core';

@Injectable()
export class CargandoService {
  estaBloqueado = false;
  cambioBloqueado: EventEmitter<boolean> = new EventEmitter();

  bloquear() {
    // this.cambioBloqueado.emit(true);
    // this.estaBloqueado = true;
  }

  desbloquear() {
    // this.estaBloqueado = false;
    // this.cambioBloqueado.emit(false);
  }

  habilitarCargando() {
    this.bloquear();
  }

  deshabilitarCargando() {
    this.desbloquear();
  }
}
