import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RutaInicioComponent} from './rutas/ruta-inicio/ruta-inicio.component';
import {RutaNoEncontradoComponent} from './rutas/ruta-no-encontrado/ruta-no-encontrado.component';
import {RutaLoginComponent} from './rutas/ruta-login/ruta-login.component';
import {EstaLogeadoGuard} from './servicios/guards/esta-logeado.guard';
import {GestionaBodegasGuard} from './servicios/guards/gestiona-bodegas.guard';
import {GestionaDepartamentosGuard} from './servicios/guards/gestiona-departamentos.guard';
import {GestionaProductosGuard} from './servicios/guards/gestiona-productos.guard';
import {GestionaInventariosGuard} from './servicios/guards/gestiona-inventarios.guard';
import {VeReportesGuard} from './servicios/guards/ve-reportes.guard';
import {GestionaUsuariosGuard} from './servicios/guards/gestiona-usuarios.guard';

const routes: Routes = [
  {
    path: 'inicio',
    component: RutaInicioComponent,
    canActivate: [EstaLogeadoGuard]
  },
  {
    path: 'login',
    component: RutaLoginComponent
  },
  {
    path: 'bodegas',
    loadChildren: () =>
      import('./modulos/bodega/bodega.module')
        .then(
          mod => mod.BodegaModule
        ),
    canActivate: [EstaLogeadoGuard, GestionaBodegasGuard]
  },
  {
    path: 'departamentos',
    loadChildren: () =>
      import('./modulos/departamento/departamento.module')
        .then(
          mod => mod.DepartamentoModule
        ),
    canActivate: [EstaLogeadoGuard, GestionaDepartamentosGuard]
  },
  {
    path: 'productos',
    loadChildren: () =>
      import('./modulos/producto/producto.module')
        .then(
          mod => mod.ProductoModule
        ),
    canActivate: [EstaLogeadoGuard, GestionaProductosGuard],
  },
  {
    path: 'usuarios',
    loadChildren: () =>
      import('./modulos/usuario/usuario.module')
        .then(
          mod => mod.UsuarioModule
        ),
    canActivate: [EstaLogeadoGuard, GestionaUsuariosGuard]
  },
  {
    path: 'inventarios',
    loadChildren: () =>
      import('./modulos/inventario/inventario.module')
        .then(
          mod => mod.InventarioModule
        ),
    canActivate: [EstaLogeadoGuard, GestionaInventariosGuard]
  },
  {
    path: 'reportes',
    loadChildren: () =>
      import('./modulos/reportes/reportes.module')
        .then(
          mod => mod.ReportesModule
        ),
    canActivate: [EstaLogeadoGuard, VeReportesGuard]
  },
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: RutaNoEncontradoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
