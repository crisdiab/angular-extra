import { Pipe, PipeTransform } from '@angular/core';
import {EstadosMantenimientoEnum} from '../../constantes/estados-mantenimiento.enum';

@Pipe({
  name: 'estadoMantenimientoPipe'
})
export class EstadoMantenimientoPipePipe implements PipeTransform {

  transform(value: 'PR'|'ER'|'PE'|'EN', ...args: unknown[]): any {
    switch (value) {
      case 'PR':
        return EstadosMantenimientoEnum.PR;
        break;
      case 'ER':
        return EstadosMantenimientoEnum.ER;
        break;
      case 'PE':
        return EstadosMantenimientoEnum.PE;
        break;
      case 'EN':
        return EstadosMantenimientoEnum.EN;
        break;

    }
  }

}
