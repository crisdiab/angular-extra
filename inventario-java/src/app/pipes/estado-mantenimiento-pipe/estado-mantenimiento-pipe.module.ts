import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EstadoMantenimientoPipePipe } from './estado-mantenimiento-pipe.pipe';



@NgModule({
    declarations: [EstadoMantenimientoPipePipe],
    exports: [
        EstadoMantenimientoPipePipe
    ],
    imports: [
        CommonModule
    ]
})
export class EstadoMantenimientoPipeModule { }
