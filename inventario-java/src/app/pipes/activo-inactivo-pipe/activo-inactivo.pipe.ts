import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'activoInactivo'
})
export class ActivoInactivoPipe implements PipeTransform {

  transform(valor: {habilitado: boolean}, ...args: unknown[]): unknown {
    return valor.habilitado ? 'Activo' : 'Inactivo'
  }

}
