import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivoInactivoPipe } from './activo-inactivo.pipe';



@NgModule({
  declarations: [ActivoInactivoPipe],
  imports: [
    CommonModule
  ],
  exports: [
    ActivoInactivoPipe
  ]
})
export class ActivoInactivoPipeModule { }
