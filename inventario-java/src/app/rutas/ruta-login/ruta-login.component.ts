import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {
  MENSAJES_VALIDACION_PASSWORD,
  MENSAJES_VALIDACION_USUARIO
} from '../../modulos/bodega/constantes/mensajes-validacion';
import {ToastrService} from 'ngx-toastr';
import {LoginService, UsuarioLogin} from '../../servicios/login.service';
import {Router} from '@angular/router';
import {CargandoService} from '../../servicios/cargando.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-ruta-login',
  templateUrl: './ruta-login.component.html',
  styleUrls: ['./ruta-login.component.scss']
})
export class RutaLoginComponent implements OnInit, OnDestroy {
  environment= environment;
  hide = true;
  formularioUsuario?: FormGroup;
  mensajesErrorUsuario: string[] = [];
  mensajesErrorPassword: string[] = [];
  subscriptores: Subscription[] = [];
  credenciales: { usuario: string, password: string } = {
    usuario: '',
    password:''
}
  private _mensajesValidacionUsuario = MENSAJES_VALIDACION_USUARIO;
  private _mensajesValidacionPassword = MENSAJES_VALIDACION_PASSWORD;

  constructor(private readonly _formBuilder: FormBuilder,
              private readonly _toasterService: ToastrService,
              private readonly _loginService: LoginService,
              private readonly _router: Router,
              private readonly _toastrService: ToastrService,
              private readonly _cargandoService: CargandoService
  ) {

  }

  ngOnInit(): void {
    this.formularioUsuario = this._formBuilder.group(
      {

        usuario: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ],
        ],
        password: ['',
          [
            Validators.required,
          ],
        ],
      });
    console.log('this.form usuario', this.formularioUsuario);
    this._escucharFormulario();
    if (this.formularioUsuario) {
      this._escucharCampoUsuario();
      this._escucharCampoPassword();
    }
  }

  ngOnDestroy(): void {
    this.subscriptores.forEach(valor => valor.unsubscribe());
  }

  private _escucharFormulario() {
    // el observable que escucha cambios
    if (this.formularioUsuario) {
      const observableCambiosFormulario$ = this.formularioUsuario.valueChanges;
      const observableFOmrulario = observableCambiosFormulario$
        .pipe(
          debounceTime(1000)
        )
        .subscribe((formulario) => {
          // console.log('escuchando cambios formulario', formulario)
          if (this.formularioUsuario?.valid) {
            console.log('valido', formulario);
            this.credenciales.usuario = formulario.usuario;
            this.credenciales.password = formulario.password
            this._toasterService.success(
              'Formulario válido',
              'Correcto',
            );
          }

        });
      this.subscriptores.push(observableFOmrulario);
    }


  }

  private _escucharCampoUsuario() {
    if (this.formularioUsuario) {
      const controlUsuario = this.formularioUsuario.get('usuario') as AbstractControl;

      const observableDeCambiosUsuario$ = controlUsuario.valueChanges;

      const respuestaObservableUsuario = observableDeCambiosUsuario$
        .pipe(
          debounceTime(1000)
        )
        .subscribe((respuesta) => {
          this._setearMensajesUsuario(controlUsuario);
        });
      this.subscriptores.push(respuestaObservableUsuario);
    }
  }

  private _setearMensajesUsuario(controlForm: AbstractControl) {
    this.mensajesErrorUsuario = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorUsuario = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' | 'minlength' | 'maxlength' = atributo as 'required' | 'minlength' | 'maxlength';
          return this._mensajesValidacionUsuario[mensajeError];
        });
    }
  }

  private _escucharCampoPassword() {
    if (this.formularioUsuario) {
      const controlPassword = this.formularioUsuario.get('password') as AbstractControl;
      const observableDeCambiosPassword$ = controlPassword.valueChanges;
      const respuestaObservablePassword = observableDeCambiosPassword$
        .pipe(
          debounceTime(2000)
        )
        .subscribe((respuesta) => {
          this._setearMensajesPassword(controlPassword);
        });
      this.subscriptores.push(respuestaObservablePassword);
    }
  }

  private _setearMensajesPassword(controlForm: AbstractControl) {
    this.mensajesErrorPassword = [];
    const tieneErrores = (controlForm.dirty || controlForm.touched) && controlForm.errors;
    if (tieneErrores) {
      const errores = controlForm.errors as { required: string; minlength: string; maxlength: string; pattern: string; };
      this.mensajesErrorPassword = Object.keys(errores)
        .map((atributo) => {
          const mensajeError: 'required' = atributo as 'required';
          return this._mensajesValidacionPassword[mensajeError];
        });
    }
  }


  login() {
   //  this._router.navigate(['/inicio']);
   //  this._loginService.guardarDatos({
   //   gestionaProductos: true,
   //   gestionaBodegas: true,
   //   gestionaDepartamentos: true,
   //   gestionaInventario: true,
   //   daMantenimiento: true,
   //    veReportes: true
   // });
   //
    this._cargandoService.habilitarCargando()
    if (this.credenciales.usuario !== '' && this.credenciales.password !== '') {
      this._loginService.login(this.credenciales)
        .subscribe(
          (usuarioLogin: UsuarioLogin) => {
            console.log('usuariologin', usuarioLogin)
            if (usuarioLogin) {
              this._loginService.guardarDatos(usuarioLogin);
              this._router.navigate(['/']);
              this._toasterService.success('Ingresó correctamente', 'Correcto');
            }
            this._cargandoService.deshabilitarCargando();
          },
          (error) => {
            this._cargandoService.deshabilitarCargando();
            this._toasterService.error('Ocurrió un error', 'Error');
          }
        );
    }

  }
}
