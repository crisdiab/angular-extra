import { Component, OnInit } from '@angular/core';
import {MENU_INICIO} from '../../constantes/menu-inicio';

@Component({
  selector: 'app-ruta-inicio',
  templateUrl: './ruta-inicio.component.html',
  styleUrls: ['./ruta-inicio.component.scss']
})
export class RutaInicioComponent implements OnInit {

  menuInicio = MENU_INICIO;
  constructor() { }

  ngOnInit(): void {
  }

}
